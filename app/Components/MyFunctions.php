<?php

class MyFunctions{

    public static function checkUserCompleteData()
    {
        $user = auth()->guard('user')->user();

        if ($user->city_id == null) {
            return true;
        }
        if ($user->age == null) {
            return true;
        }
        if ($user->marital_status == null) {
            return true;
        }
        if ($user->height == null) {
            return true;
        }
        if ($user->weight == null) {
            return true;
        }
        if ($user->education == null) {
            return true;
        }
        if ($user->employment_status == null) {
            return true;
        }
        if ($user->religious == null) {
            return true;
        }
        if ($user->dowry == null) {
            return true;
        }
        if ($user->dowry_cash == null) {
            return true;
        }
        if (!isset($user->child)) {
            return true;
        }
        if ($user->story == null) {
            return true;
        }

        return false;
    }

    public static function convertToPersianNumber($num, $sub_str = false)
    {

        $number = $num;
        if ($sub_str == true) {
            settype($num, "String");
            $n = strlen($num);
            $i = 0;
            $help = $n % 3;
            while ($help != 0) {
                $num = '0' . $num;
                $i++;
                $n = strlen($num);
                $help = $n % 3;
            }
            $arr = str_split($num, 3);
            $str = "";
            foreach ($arr as $index) {
                $str = $str . "," . $index;
            }
            $i++;
            $number = substr($str, $i);

        }

        $english = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
        $persian = array('٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩');

        $convertedStr = str_replace($english, $persian, $number);
        return $convertedStr;
    }
}