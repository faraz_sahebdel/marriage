<?php
namespace App\Components;

use Kavenegar\Exceptions\ApiException;
use Kavenegar\Exceptions\HttpException;
use Kavenegar\KavenegarApi;

class MyKavenegar extends KavenegarApi {
    public function __construct($apiKey, $insecure = false) {
        parent::__construct($apiKey, $insecure);
    }

    public static function instance() {
        $apiKey = env('KAVENEGAR_API_KEY');
        return new self($apiKey);
    }

    public function lookup($receptor, $token, $token2, $token3, $template) {
        try {
            $result = $this->VerifyLookup($receptor, $token, $token2, $token3, $template, "sms");
            if ($result) {
                return true;
            }
        } catch (ApiException $e) {
            \Log::error($e->errorMessage());
        } catch (HttpException $e) {
            \Log::error($e->errorMessage());
        }

        abort(500, 'مشکلی در ارسال اس ام اس وجود دارد');
    }

    public function info(){
        try {
            $result = $this->AccountInfo();
            if ($result) {
                return $result;
            }
        } catch (ApiException $e) {
            \Log::error($e->errorMessage());
        } catch (HttpException $e) {
            \Log::error($e->errorMessage());
        }
        return false;
        // abort(500, 'خطا در ارتباط');
    }
}
