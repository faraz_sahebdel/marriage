<?php

namespace App\Components;


use App\Model\Order;
use GuzzleHttp\Client;

class ZarinPal
{

    protected $order;
    protected $merchantId;
    protected $callback;

    protected $request;

    public function __construct(Order $order)
    {


        $this->order = $order;

        $this->merchantId = env('ZARINPAL_MERCHANT_ID');

        $this->callback = route('user.order.callback', ['id' => encrypt($this->order->id)]);


        $this->request = new Client();
    }

    /**
     * Purchase Invoice.
     *
     * @return string
     * @throws \SoapFault
     */
    public function purchase()
    {
        // $client = new \SoapClient('https://sandbox.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);

        $client = new \SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);

        $result = $client->PaymentRequest([
            'MerchantID' => $this->merchantId,
            'Amount' => $this->order->price,
            'CallbackURL' => $this->callback,
            'Description' => 'خرید',
            // 'Email' => $this->order->user->email,
            'Mobile' => $this->order->user->mobile
        ]);

        if (intval($result->Status) !== 100) {
            abort(500, $this->notVerified($result->Status));
        }

        $this->order->authority = $result->Authority;
        $this->order->save();

        return 'https://www.zarinpal.com/pg/StartPay/' . $result->Authority;

        // return 'https://sandbox.zarinpal.com/pg/StartPay/' . $result->Authority;
    }


    public function verify($Authority)
    {
        $data = [
            'MerchantID' => $this->merchantId,
            'Authority' => $Authority,
            'Amount' => $this->order->price,
        ];

        //        $client = new \SoapClient('https://sandbox.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);
        $client = new \SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);

        $result = $client->PaymentVerification($data);

        if ($result->Status == 100) {
            return $result->RefID;
        }

        return NULL;
    }

    private function notVerified($status)
    {
        $translations = [
            "-1" => "اطلاعات ارسال شده ناقص است.",
            "-2" => "IP و يا مرچنت كد پذيرنده صحيح نيست",
            "-3" => "با توجه به محدوديت هاي شاپرك امكان پرداخت با رقم درخواست شده ميسر نمي باشد",
            "-4" => "سطح تاييد پذيرنده پايين تر از سطح نقره اي است.",
            "-11" => "درخواست مورد نظر يافت نشد.",
            "-12" => "امكان ويرايش درخواست ميسر نمي باشد.",
            "-21" => "هيچ نوع عمليات مالي براي اين تراكنش يافت نشد",
            "-22" => "تراكنش نا موفق ميباشد",
            "-33" => "رقم تراكنش با رقم پرداخت شده مطابقت ندارد",
            "-34" => "سقف تقسيم تراكنش از لحاظ تعداد يا رقم عبور نموده است",
            "-40" => "اجازه دسترسي به متد مربوطه وجود ندارد.",
            "-41" => "اطلاعات ارسال شده مربوط به AdditionalData غيرمعتبر ميباشد.",
            "-42" => "مدت زمان معتبر طول عمر شناسه پرداخت بايد بين 30 دقيه تا 45 روز مي باشد.",
            "-54" => "درخواست مورد نظر آرشيو شده است",
            "101" => "عمليات پرداخت موفق بوده و قبلا PaymentVerification تراكنش انجام شده است.",
        ];
        if (array_key_exists($status, $translations)) {
            return $translations[$status];
        }

        return 'خطای ناشناخته ای رخ داده است.';
    }
}
