<?php

namespace App\Http\Controllers\Admin;

use App\Components\MyKavenegar;
use App\Http\Controllers\Controller;
use App\Model\Admin;
use App\Model\Order;
use App\User;
use Carbon\Carbon;
use Hash;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        

        // auth('user')->setDefaultDriver('admin');

        // dd(123);
        $this->middleware('admin.auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $data['info'] = MyKavenegar::instance()->info();

        return view('admin.dashboard', compact('data'));
    }

    public function setting()
    {
        return view('admin.setting');
    }

    public function resetPassword(Request $request)
    {
        $request->validate([
            'old_password' => ['required'],
            'password' => ['required', 'confirmed', 'min:8'],
        ]);

        if (!Hash::check($request->old_password, auth()->guard('admin')->user()->password)) {
            return back()->with('error', 'رمز عبور قبلی اشتباه است.');
        }

        $admin = Admin::find(auth('admin')->user()->id);

        $admin->password = Hash::make($request->password);
        if ($admin->save()) {

            auth('admin')->logout();

            return redirect()->route('admin.login.form');
        }

        return back();
    }


    public function financialAll()
    {

        $data['all'] = Order::with('package')->where('status', 2)->sum('price');
        $data['month'] = Order::with('package')->where('status', 2)->whereMonth('created_at', Verta::subMonth(1)->formatGregorian('m'))->sum('price');
        $data['success'] = Order::with('package')->where('status', 2)->count();

        return view('admin.financial.index', compact('data'));
    }

    public function financialSuccess()
    {
        return view('admin.financial.success');
    }

    public function showDataTableFinancialSuccess(Request $request)
    {
        $orders = Order::with('user')->where('status', 2)->latest()->select('orders.*');

        return datatables()->eloquent($orders)
            ->editColumn('created_at', function ($orders) {
                return Verta($orders->created_at)->format('Y/m/d H:i');
            })
            ->editColumn('user.nick_name', function ($orders) {
                return '<a href="' . route('admin.user.show', $orders->user->id) . '">' . $orders->user->nick_name . '</a>';
            })
            ->rawColumns(['user.nick_name' => 'user.nick_name'])
            ->make(true);
    }

    public function financialFail()
    {
        return view('admin.financial.fail');
    }

    public function showDataTableFinancialFail(Request $request)
    {
        $orders = Order::with('user')->where('status', 1)->latest()->select('orders.*');

        return datatables()->eloquent($orders)
            ->editColumn('created_at', function ($orders) {
                return Verta($orders->created_at)->format('Y/m/d H:i');
            })
            ->editColumn('user.nick_name', function ($orders) {
                return '<a href="' . route('admin.user.show', $orders->user->id) . '">' . $orders->user->nick_name . '</a>';
            })
            ->rawColumns(['user.nick_name' => 'user.nick_name'])
            ->make(true);
    }
}
