<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Admin;
use App\Model\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use function GuzzleHttp\Promise\all;

class AdminManagerController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.admins.index');
    }

    public function showDataTable(Request $request)
    {

        $admin = Admin::query();

        return datatables()->eloquent($admin)
            ->addColumn('action', function (Admin $admin) {
                return '<a href="' . route('admin.admins.show', $admin) . '">مشاهده</a>';
            })
            ->rawColumns(['action' => 'action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.admins.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:admins',
            'password' => 'required|min:8',
        ]);


        $admin = new Admin();

        $admin->fill($request->all());
        if ($admin->save()) {
            return redirect()->route('admin.admins.index')->with('success', trans('message.success_save'));
        }
        return redirect()->route('admin.admins.index')->with('error', trans('message.success_error'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function show(Admin $admin)
    {
        return view('admin.admins.show', compact('admin'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $admin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admin $admin)
    {
        $admin->fill($request->all());
        $admin->save();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Admin $admin)
    {
        $admin->delete();

        return redirect()->route('admin.admins.index')->with('success', trans('message.user_success_delete'));
    }

    public function permission(Admin $admin)
    {

        // dd($admin->can('user-edit'));

        $permissions = Permission::all();

        return view('admin.admins.permission', compact('admin', 'permissions'));
    }

    public function permissionUpdate(Request $request, Admin $admin)
    {
        $admin->permissions()->sync($request->permissions);
        return back();
    }
}
