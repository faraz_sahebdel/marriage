<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Alert;
use Illuminate\Http\Request;

class AlertController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth');
    }

    public function index()
    {
        return view('admin.alert.index');
    }

    public function showDataTableAlert(Request $request)
    {
        $alert = Alert::latest();

        return datatables()->eloquent($alert)
            // ->editColumn('created_at', function ($alert) {
            //     return Verta($alert->created_at)->format('Y/m/d H:i');
            // })
            ->editColumn('action', function ($alert) {
                return '<a href="' . route('admin.alert.destroy', $alert) . '">حذف از  لیست اعلان ها</a>';

                // return '<a href="' . route('admin.user.show', $alert->user->id) . '">' . $alert->user->nick_name . '</a>';
            })
            ->rawColumns(['action' => 'action'])
            ->make(true);
    }

    public function create()
    {
        return view('admin.alert.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'content' => 'required'
        ]);

        $alert = new Alert();
        $alert->content = $request->content;
        if ($alert->save()) {
            return redirect()->route('admin.alert.index')->with('success', trans('message.success_created'));
        }
        return redirect()->route('admin.alert.index')->with('error', trans('message.error_create'));
    }

    public function destroy(Alert $alert)
    {
        $alert->delete();
        return back();
    }
}
