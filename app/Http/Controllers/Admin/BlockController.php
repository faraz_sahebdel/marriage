<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Admin;
use App\Model\Block;
use App\Model\User;
use Illuminate\Http\Request;

class BlockController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.block.index');
    }

    public function showDataTable(Request $request)
    {
        $block = Block::with('user_blocker', 'user_blocked')->select('blocks.*');

        return datatables()->eloquent($block)
            ->make(true);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function adminBlockIndex()
    {
        return view('admin.block.admin_block');
    }

    public function adminBlockShowDataTable(Request $request)
    {
        $query = User::select()->where('block_admin', 1);

        return datatables()->eloquent($query)
            ->addColumn('action', function (User $user) {
                return '<a href="' . route('admin.admin.block.delete', $user) . '">حذف از  لیست مسدودی</a>';
            })
            ->rawColumns(['action' => 'action'])
            ->make(true);
    }

    public function adminBlockDelete(User $user)
    {
        $user->block_admin = 0;
        $user->update();
        return back()->with('success', trans('message.success_delet_block_admin'));
    }

    public function adminBlockAdd(User $user)
    {
        $user->block_admin = 1;
        $user->update();
        return back()->with('success', trans('message.success_add_block_admin'));
    }

}
