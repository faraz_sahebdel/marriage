<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Config;
use Illuminate\Http\Request;

class ConfigController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $config = Config::findOrFail(1);
        return view('admin.config.index', compact('config'));
    }

    public function update(Request $request)
    {
        $config = Config::findOrFail(1);
        $config->fill($request->all());
        if ($config->update()) {
            return redirect()->route('admin.config')->with('success', trans('message.success_updated'));
        }
        return redirect()->route('admin.config')->with('error', trans('message.error_created'));
    }
}
