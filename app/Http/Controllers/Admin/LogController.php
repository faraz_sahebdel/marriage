<?php

namespace App\Http\Controllers\Admin;

use App\Exports\LogExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\Activitylog\Models\Activity;

class LogController extends Controller
{


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd(\App\Model\User::findOrFail(2)->nick_name);
        return view('admin.log.index');
    }

    public function showDataTable(Request $request)
    {

        $log = Activity::query();

        return datatables()->of($log)
            ->editColumn('subject_id', function ($log) {
                if ($log->subject_type == 'App\Model\Admin') {
                    return $log->subject_type::findOrFail($log->subject_id)->name;
                } else if ($log->subject_type == 'App\Model\User') {
                    return $log->subject_type::findOrFail($log->subject_id)->nick_name;
                } else if ($log->subject_type == 'App\Model\Office') {
                    return $log->subject_type::findOrFail($log->subject_id)->name;
                }
                return explode("\\", $log->subject_type)[2];
            })
            ->editColumn('causer_id', function ($log) {
                if ($log->causer_type == 'App\Model\Admin') {
                    return $log->causer_type::findOrFail($log->causer_id)->name;
                } else if ($log->causer_type == 'App\Model\User') {
                    return $log->causer_type::findOrFail($log->causer_id)->nick_name;
                } else if ($log->causer_type == 'App\Model\Office') {
                    return $log->causer_type::findOrFail($log->causer_id)->name;
                }
                return $log->causer_id;
            })
            ->editColumn('created_at', function ($log) {
                return Verta($log->created_at)->format('Y/m/d H:i');
            })
            ->rawColumns(['action' => 'action'])
            ->make(true);
    }

    public function export()
    {
        return Excel::download(new LogExport, 'log.xlsx');
    }
}
