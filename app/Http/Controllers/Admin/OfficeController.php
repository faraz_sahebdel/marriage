<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\City;
use App\Model\Office;
use Illuminate\Http\Request;

class OfficeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.office.index');
    }

    public function showDataTable(Request $request)
    {

        $offices = Office::with('city')->select('offices.*');

        return datatables()->eloquent($offices)
            ->editColumn('confirmed_admin', function (Office $offices) {
                if ($offices->confirmed_admin == 0) {
                    return 'تایید نشده';
                } else if ($offices->confirmed_admin == 1) {
                    return 'تایید شده';
                }
            })
            ->addColumn('action', function (Office $offices) {
                return '<a href="' . route('admin.office.show', $offices) . '">مشاهده</a>';
            })
            ->rawColumns(['action' => 'action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Office  $office
     * @return \Illuminate\Http\Response
     */
    public function show(Office $office)
    {
        $cities = City::with('cities')->where('parent_id', null)->get();

        return view('admin.office.show', compact('office', 'cities'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Office  $office
     * @return \Illuminate\Http\Response
     */
    public function edit(Office $office)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Office  $office
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Office $office)
    {
        $office->confirmed_admin = $request->confirmed_admin;

        if ($office->update($request->all())) {

            return back()->with('success', trans('message.success_updated'));
        }

        return back()->with('error', trans('message.error_update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Office  $office
     * @return \Illuminate\Http\Response
     */
    public function destroy(Office $office)
    {
        $office->delete();

        return redirect('admin.office.index');
    }
}
