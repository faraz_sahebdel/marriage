<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Report;
use Illuminate\Http\Request;

class ReportController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.report.index');
    }

    public function showDataTable(Request $request)
    {
        $report = Report::with('user_reporter', 'user_report')->select('reports.*');

        return datatables()->eloquent($report)
            ->editColumn('report', function (Report $report) {
                return Report::REPORT_LIST[$report->report];
            })
            ->make(true);
    }
}
