<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Office;
use App\Model\Request as ModelRequest;
use Illuminate\Http\Request;

class RequestController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth');
    }

    public function index()
    {
        return view('admin.request.index');
    }

    public function showDataTableRequest(Request $request)
    {
        $requests = ModelRequest::with(['user', 'user_acceptor', 'office'])->latest()->select('requests.*');

        return datatables()->eloquent($requests)
            ->editColumn('status', function ($requests) {
                $result = '';
                if ($requests->status == '0') {
                    $result = 'در حال انتظار';
                } else if ($requests->status == '1') {
                    $result = 'رد شده';
                } else if ($requests->status == '2') {
                    $result = 'قبول شده';
                }
                return $result;
            })
            ->editColumn('user.nick_name', function ($requests) {
                return '<a href="' . route('admin.user.show', $requests->user) . '">' . $requests->user->nick_name . '</a>';
            })
            ->editColumn('user_acceptor.nick_name', function ($requests) {
                return '<a href="' . route('admin.user.show', $requests->user_acceptor) . '">' . $requests->user_acceptor->nick_name . '</a>';
            })
            ->addColumn('action', function (ModelRequest $requests) {
                if ($requests->office_id != null) {
                    // route('admin.offuce.show', $requests->id)
                    return '<a href="' . route('admin.office.show', $requests->office) . '">' . $requests->office->name . '</a>';
                }
                return '<a href="' . route('admin.request.offuce.send', $requests->id) . '">ارسال به دفتر</a>';
            })
            ->editColumn('created_at', function ($requests) {
                return Verta($requests->created_at)->format('Y/m/d');
            })
            ->rawColumns(['action' => 'action', 'user.nick_name' => 'user.nick_name', 'user_acceptor.nick_name' => 'user_acceptor.nick_name'])
            ->make(true);
    }

    public function officeSend(ModelRequest $request)
    {
        $offices = Office::all();

        return view('admin.request.office', compact('offices', 'request'));
    }

    public function officeSendSave(Request $request, ModelRequest $model_request)
    {
        $model_request->office_id = $request->name;
        $model_request->save();
        return redirect()->route('admin.request.index');
    }
}
