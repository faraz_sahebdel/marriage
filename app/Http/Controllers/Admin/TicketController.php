<?php

namespace App\Http\Controllers\Admin;

use App\Components\MyKavenegar;
use App\Http\Controllers\Controller;
use App\Model\Ticket;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Http\Request;

class TicketController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth');
    }

    public function index()
    {
        return view('admin.ticket.index');
    }

    public function showDataTableTicket(Request $request)
    {
        $tikets = Ticket::where('parent_id', null)->latest();

        return datatables()->eloquent($tikets)
            ->editColumn('status', function ($tikets) {
                $result = '';
                if ($tikets->status == Ticket::STATUS_USER_RESPONSE) {
                    $result = '<span class="text-info">پاسخ کاربر</span>';
                } else if ($tikets->status == Ticket::STATUS_ADMIN_RESPONSE) {
                    $result = '<span class="text-warning">پاسخ مدیر</span>';
                } else if ($tikets->status == Ticket::STATUS_CLOSED_TICKET) {
                    $result = '<span class="text-success">بسته شده</span>';
                }
                return $result;
            })
            ->editColumn('created_at', function ($tikets) {
                return Verta($tikets->created_at)->format('Y/m/d H:i');
            })
            ->addColumn('action', function ($tikets) {
                return '<a href="' . route('admin.ticket.send', $tikets) . '" class="text-success">پاسخ</a>';
            })
            ->rawColumns(['action' => 'action', 'status' => 'status'])
            ->make(true);
    }

    public function send(Ticket $ticket)
    {
        $tickets = Ticket::where('parent_id', $ticket->id)->get();
        return view('admin.ticket.send', compact('ticket', 'tickets'));
    }

    public function sendData(Request $request)
    {
        $ticket_parent = Ticket::where('id', $request->ticket)->first();
        if ($ticket_parent) {
            $ticket = new Ticket();
            $ticket->user_id = $ticket_parent->user_id;
            $ticket->admin_id = auth('admin')->user()->id;
            $ticket->parent_id = $request->ticket;
            $ticket->content = $request->message;
            $ticket->status = '1';
            if ($ticket->save()) {
                Ticket::where('id', $request->ticket)->update(['status' => '1']);

                MyKavenegar::instance()->lookup($request->user->mobile, 'جواب تیکت', null, null, 'ezdsignup');

                return back();
            }
        }
        return back();
    }
}
