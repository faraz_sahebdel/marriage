<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserAdminCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Model\City;
use App\Model\User;
use Hash;
use Illuminate\Auth\AuthManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Str;

class UserController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.user.index');
    }

    public function showDataTable(Request $request)
    {

        $user = User::with('city')->select('users.*');

        return datatables()->eloquent($user)
            ->addColumn('action', function (User $user) {
                return '<a href="' . route('admin.user.show', $user) . '">مشاهده</a>';
            })
            ->rawColumns(['action' => 'action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cities = City::with('cities')->where('parent_id', null)->get();

        return view('admin.user.create', compact('cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserAdminCreateRequest $request)
    {

        $data = [];
        foreach ($request->all() as $key => $value) {
            if($key == 'password'){
                $data[$key] = Hash::make($value);
                continue;
            }
            $data[$key] = strip_tags($value);
        }

        $data['dowry_cash'] = 0;
        $data['child'] = 0;

        if (User::create($data)) {

            return redirect()->route('admin.user.index')->with('success', trans('message.success_created'));
        }

        return back()->with('warning', trans('message.error_create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $cities = City::with('cities')->where('parent_id', null)->get();

        return view('admin.user.show', compact('user', 'cities'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserAdminCreateRequest $request, User $user)
    {

        $user = User::findOrFail($user->id);

        $user->gender = $request->gender;

        if ($user->update($request->all())) {

            return back()->with('success', trans('message.success_updated'));
        }

        return back()->with('error', trans('message.error_update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return redirect('admin.user.index');
    }
}
