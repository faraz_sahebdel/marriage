<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Model\Office;
use App\Providers\RouteServiceProvider;
use Hash;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest')->except('logout');

        $this->middleware('admin.redirect')->except('adminLogout');
        $this->middleware('user.redirect')->except('userLogout');
        $this->middleware('office.redirect')->except('officeLogout');
    }



    /** User Login Config
     * 
     */
    public function showUserLoginForm()
    {
        return view('auth.user_login');
    }

    public function userLogin(Request $request)
    {
        $request->validate([
            'mobile'   => 'required|regex:/(09)[0-9]{9}/|digits:11|numeric',
            'password' => 'required|min:8',
        ]);


        if (auth('user')->attempt(['mobile' => $request->mobile, 'password' => $request->password], $request->remember)) {

            return redirect()->route('user.index');
        }

        return back()->withInput($request->only('mobile', 'remember'));
    }

    public function userLogout()
    {
        auth('user')->logout();

        return redirect()->route('user.login.form');
    }


    /**
     * Admin Login Config
     * 
     */

    public function showAdminLoginForm()
    {
        return view('auth.admin_login');
    }

    public function adminLogin(Request $request)
    {
        $request->validate([
            'email'   => 'required|email',
            'password' => 'required|min:8'
        ]);


        if (auth('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {

            return redirect()->route('admin.index');
        }

        return back()->withInput($request->only('email', 'remember'));
    }

    public function adminLogout()
    {
        auth('admin')->logout();

        return redirect()->route('admin.login.form');
    }


    /** Office Login Config
     * 
     */
    public function showOfficeLoginForm()
    {
        return view('auth.office_login');
    }

    public function officeLogin(Request $request)
    {
        $request->validate([
            'mobile'   => 'required|regex:/(09)[0-9]{9}/|digits:11|numeric',
            'password' => 'required|min:8',
        ]);

        if (auth('office')->attempt(['mobile' => $request->mobile, 'password' => $request->password], $request->remember)) {

            if(auth('office')->user()->confirmed_admin == 0){
                auth('office')->logout();
                return redirect()->route('office.login.form')->with('warning', trans('message.office_admin_confirme'));
            }
            return redirect()->route('office.index');
        }

        return back()->withInput($request->only('mobile', 'remember'));
    }

    public function officeLogout()
    {
        auth('office')->logout();

        return redirect()->route('office.login.form');
    }
}
