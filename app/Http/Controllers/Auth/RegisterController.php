<?php

namespace App\Http\Controllers\Auth;

use App\Components\MyKavenegar;
use App\Http\Controllers\Controller;
use App\Model\City;
use App\Model\Config;
use App\Model\Office;
use App\Providers\RouteServiceProvider;
use App\Model\User;
use Auth;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.redirect')->except('adminLogout');
        $this->middleware('user.redirect')->except('userLogout');
        $this->middleware('office.redirect')->except('officeLogout');
    }

    public function register(Request $request)
    {
        $request->validate([
            'nick_name' => ['required', 'string', 'max:255'],
            'mobile' => ['required', 'regex:/(09)[0-9]{9}/', 'digits:11', 'numeric', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        $config = Config::first();

        $user = User::create([
            'nick_name' => $request->nick_name,
            'mobile' => $request->mobile,
            'code' => Str::random(5),
            'gender' => $request->gender,
            'password' => Hash::make($request->password),
            'sms_count' => $config->free_sms,
            'image' => ($request->gender == 0 ? '0.png' : '1.png'),
        ]);

        auth('user')->login($user);

        MyKavenegar::instance()->lookup($request->mobile, $user->code, null, null, 'ezdsignup');

        return redirect()->route('user.index');
    }

    public function showRegistrationForm()
    {
        return view('auth.user_register');
    }


    /**
     * Office Register Config
     */
    public function officeRegister(Request $request)
    {

        $request->validate([
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'mobile' => ['required', 'regex:/(09)[0-9]{9}/', 'digits:11', 'numeric', 'unique:offices'],
            'phone' => ['required', 'digits:11', 'numeric'],
            'address' => ['required'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);


        Office::create([
            'city_id' => $request->city_id,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'mobile' => $request->mobile,
            'phone' => $request->phone,
            'address' => $request->address,
            'password' => Hash::make($request->password),
        ]);

        // auth('user')->login($office);

        // event SMS

        return redirect()->route('office.login')->with('success', trans('message.success_sginup_office'));
    }

    public function showOfficeRegistrationForm()
    {
        $cities = City::with('cities')->where('parent_id', null)->get();

        return view('auth.office_register', compact('cities'));
    }
}
