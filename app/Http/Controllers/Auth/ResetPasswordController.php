<?php

namespace App\Http\Controllers\Auth;

use App\Components\MyKavenegar;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Providers\RouteServiceProvider;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;


    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showResetForm(Request $request, $token = null)
    {
        return view('auth.passwords.user_reset')->with(
            ['token' => $token, 'mobile' => $request->mobile]
        );
    }


    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function reset(Request $request)
    {
        $request->validate([
            'mobile'   => 'required|regex:/(09)[0-9]{9}/|digits:11|numeric',
        ]);

        if (User::where('mobile', $request->mobile)->exists()) {

            $token = Str::random(5);
            DB::table('password_resets')->insert([
                'mobile' => $request->mobile,
                'token' => $token,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);

            MyKavenegar::instance()->lookup($request->mobile, $token, null, null, 'ezdsignup');

            return redirect()->route('password.reset.token', $request->mobile);
        }
        return back();
    }

    public function showTokenForm($mobile)
    {
        return view('auth.passwords.user_password_reset_code', compact('mobile'));
    }

    public function tokenConfirmed(Request $request)
    {
        $request->validate([
            'mobile' => 'required|regex:/(09)[0-9]{9}/|digits:11|numeric',
            'token' => 'required|string|max:5',
            'password' => 'required|min:8|confirmed',
        ]);

        if ($password_resets = DB::table('password_resets')->where('mobile', $request->mobile)->where('token', $request->token)->latest()->first()) {

            // dd($password_resets->created_at);
            if (Carbon::now()->subMinute('10')->format('Y-m-d H:i:s') > $password_resets->created_at) {
                DB::table('password_resets')->where('mobile', $request->mobile)->where('token', $request->token)->delete();
                return redirect()->route('password.request');
            }

            if (DB::table('users')->where('mobile', $request->mobile)->update(['password' => Hash::make($request->password)])) {
                return redirect()->route('user.login');
            }
        }
        return back();
    }
}
