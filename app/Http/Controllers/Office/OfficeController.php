<?php

namespace App\Http\Controllers\Office;

use App\Http\Controllers\Controller;
use App\Model\City;
use App\Model\Office;
use App\Model\Order;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class OfficeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('office.auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        return view('office.dashboard');
    }

    public function info()
    {
        $cities = City::with('cities')->where('parent_id', null)->get();

        $office = Office::where('id', auth('office')->user()->id)->first();
        return view('office.info', compact('cities', 'office'));
    }

    public function infoUpdate(Request $request)
    {
        $request->validate([
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'mobile' => [
                'required',
                'regex:/(09)[0-9]{9}/',
                'digits:11', 'numeric',
                Rule::unique('offices')->ignore(auth('office')->user()->id, 'id')
            ],
            'phone' => ['required', 'digits:11', 'numeric'],
            'address' => ['required'],
        ]);

        $office = Office::findOrFail(auth('office')->user()->id);
        $office->confirmed_admin = 0;

        $office->update($request->all());
        if ($office->save()) {
            auth('office')->logout();
            return redirect()->route('office.login');
        }
        return back();
    }
}
