<?php

namespace App\Http\Controllers\Office;

use App\Http\Controllers\Controller;
use App\Model\Request as ModelRequest;
use Illuminate\Http\Request;

class RequestController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('office.auth');
    }


    public function index()
    {
        return view('office.request.index');
    }

    public function showDataTableRequest(Request $request)
    {
        $requests = ModelRequest::with(['user', 'user_acceptor'])->where('office_id', auth('office')->user()->id)->latest()->select('requests.*');

        return datatables()->eloquent($requests)
            ->editColumn('status', function ($requests) {
                $result = '';
                if ($requests->status == '0') {
                    $result = 'در حال انتظار';
                } else if ($requests->status == '1') {
                    $result = 'رد شده';
                } else if ($requests->status == '2') {
                    $result = 'قبول شده';
                }
                return $result;
            })
            // ->editColumn('created_at', function ($requests) {
            //     return Verta($requests->created_at)->format('Y/m/d');
            // })
            // ->rawColumns(['action' => 'action'])
            ->make(true);
    }
}
