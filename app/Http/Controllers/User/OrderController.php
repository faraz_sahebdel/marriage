<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Model\Order;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Http\Request;

class OrderController extends Controller
{


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('user.auth');
    }


    public function index()
    {
        return view('user.order.index');
    }


    public function showDataTableOrder(Request $request)
    {
        $order = Order::where('user_id', auth('user')->user()->id)->latest();

        return datatables()->eloquent($order)
            ->editColumn('status', function ($order) {
                $result = '';
                if ($order->status == Order::STATUS_FAILED) {
                    $result = '<span class="text-danger">ناموفق</span>';
                } else if ($order->status == Order::STATUS_WATING) {
                    $result = '<span class="text-warning">منتظر</span>';
                } else if ($order->status == Order::STATUS_SUCCESS) {
                    $result = '<span class="text-success">موفق</span>';
                }
                return $result;
            })
            ->editColumn('created_at', function ($order) {
                return Verta($order->created_at)->format('Y/m/d - H:i');
            })
            // ->addColumn('action', function (Block $order) {
            //     return '<a href="' . route('user.block.delete', $order) . '" class="text-danger">حذف از مسدود</a>';
            // })
            ->rawColumns(['status' => 'status'])
            ->make(true);
    }
}
