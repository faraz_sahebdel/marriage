<?php

namespace App\Http\Controllers\User;

use App\Components\ZarinPal;
use App\Http\Controllers\Controller;
use App\Model\Order;
use App\Model\Package;
use Illuminate\Http\Request;

class PackageController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('user.auth');
    }

    
    public function index()
    {
        // return redirect('https://www.google.com');

        $pakages = Package::all();
        return view('user.package.index', compact('pakages'));
    }

    public function packageBuy(Package $package)
    {
        $order = new Order();
        $order->user_id = auth('user')->user()->id;
        $order->package_id = $package->id;
        $order->price = $package->price;
        $order->count = $package->count;
        $order->save();

        $zarinPal = new ZarinPal($order);

        return redirect($zarinPal->purchase());
    }

    public function callback(Request $request, $id)
    {
        $order = Order::where('id', decrypt($id))->where('user_id', auth('user')->user()->id)->first();

        if ($request->Status != 'OK') {
            $order->status = Order::STATUS_FAILED;
            $order->save();
            return redirect()->route('user.order.index')->with('error', trans('message.payment_failed'));
        }

        $zarinPal = new ZarinPal($order);
        $ref = $zarinPal->verify($request->Authority);

        if (!$ref) {
            $order->status = Order::STATUS_FAILED;
            $order->save();
            return redirect()->route('user.order.index')->with('error', trans('message.payment_failed'));
        }

        $order->ref_id = $ref;
        $order->status = Order::STATUS_SUCCESS;

        if($order->save()){
            $order->user->sms_count = $order->user->sms_count + $order->count;
            $order->user->save();
        }
        return redirect()->route('user.order.index')->with('success', trans('message.payment_success'));
    }
}
