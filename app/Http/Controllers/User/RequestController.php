<?php

namespace App\Http\Controllers\User;

use App\Components\MyKavenegar;
use App\Http\Controllers\Controller;
use App\Model\Block;
use App\Model\Like;
use App\Model\Request as ModelRequest;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Http\Request;

class RequestController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('user.auth');
    }

    public function requestGet()
    {
        return view('user.request.get');
    }

    public function showDataTableRequestGet(Request $request)
    {
        $requests = ModelRequest::with('user')->where('user_acceptor_id', auth('user')->user()->id)->latest()->select('requests.*');

        return datatables()->eloquent($requests)
            ->editColumn('status', function ($requests) {
                $result = '';
                if ($requests->status == '0') {
                    $result = 'در حال انتظار';
                } else if ($requests->status == '1') {
                    $result = 'رد شده';
                } else if ($requests->status == '2') {
                    $result = 'قبول شده';
                }
                return $result;
            })
            ->editColumn('user.nick_name', function ($requests) {
                return '<a href="' . route('user.show', $requests->user) . '">' . $requests->user->nick_name . '</a>';
            })
            ->addColumn('action', function (ModelRequest $requests) {
                if ($requests->status == 0) {
                    $result = '<a href="' . route('user.request.accept', $requests) . '" class="text-success">می پذیرم</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    $result .= '<a href="' . route('user.request.reject', $requests) . '" class="text-danger">نمی پذیرم</a>';
                    return $result;
                } else if ($requests->status == 1) {
                    return '<span class="text-danger">نپذیرفتم</span>';
                } else if ($requests->status == 2) {
                    return '<span class="text-success">پذیرفتم</span>';
                }
            })
            ->editColumn('created_at', function ($requests) {
                return Verta($requests->created_at)->format('Y/m/d');
            })
            ->rawColumns(['action' => 'action', 'user.nick_name' => 'user.nick_name'])
            ->make(true);
    }

    public function requestAccept(ModelRequest $request)
    {
        if ($request->user_acceptor_id == auth('user')->user()->id && $request->status == 0) {
            $request->status = 2;
            $request->save();

            MyKavenegar::instance()->lookup($request->user->mobile, 'قبول درخواست', null, null, 'ezdsignup');

            return back()->with('success', trans('message.success_request_accepted'));
        }
        return back();
    }

    public function requestReject(ModelRequest $request)
    {
        if ($request->user_acceptor_id == auth('user')->user()->id && $request->status == 0) {
            $request->status = 1;
            $request->save();
            
            MyKavenegar::instance()->lookup($request->user->mobile, 'رد درخواست', null, null, 'ezdsignup');

            return back()->with('success', trans('message.success_request_eject'));
        }
        return back();
    }

    public function requestSend()
    {
        return view('user.request.send');
    }

    public function showDataTableRequestSend(Request $request)
    {
        $requests = ModelRequest::with('userAcceptor')->where('user_applicant_id', auth('user')->user()->id)->latest()->select('requests.*');

        return datatables()->eloquent($requests)
            ->editColumn('status', function ($requests) {
                $result = '';
                if ($requests->status == '0') {
                    $result = 'در حال انتظار';
                } else if ($requests->status == '1') {
                    $result = 'رد شده';
                } else if ($requests->status == '2') {
                    $result = 'قبول شده';
                }
                return $result;
            })
            ->editColumn('userAcceptor.nick_name', function ($requests) {
                return '<a href="' . route('user.show', $requests->userAcceptor) . '">' . $requests->userAcceptor->nick_name . '</a>';
            })
            ->addColumn('action', function (ModelRequest $requests) {
                if ($requests->status == 0) {
                    $result = '<a href="' . route('user.request.accept', $requests) . '" class="text-success">می پذیرم</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    $result .= '<a href="' . route('user.request.reject', $requests) . '" class="text-danger">نمی پذیرم</a>';
                    return $result;
                } else if ($requests->status == 1) {
                    return '<span class="text-danger">قبول نکرده</span>';
                } else if ($requests->status == 2) {
                    return '<span class="text-success">قبول کرده</span>';
                }
            })
            ->editColumn('created_at', function ($requests) {
                return Verta($requests->created_at)->format('Y/m/d');
            })
            ->rawColumns(['action' => 'action', 'userAcceptor.nick_name' => 'userAcceptor.nick_name'])
            ->make(true);
    }

    public function favorite()
    {
        return view('user.request.favorite');
    }


    public function showDataTableFavorite(Request $request)
    {
        $likes = Like::with('user')->where('user_liker_id', auth('user')->user()->id)->latest()->select('likes.*');

        return datatables()->eloquent($likes)
            ->editColumn('user.nick_name', function ($requests) {
                return '<a href="' . route('user.show', $requests->user) . '">' . $requests->user->nick_name . '</a>';
            })
            ->rawColumns(['user.nick_name' => 'user.nick_name'])
            ->make(true);
    }

    public function block()
    {
        return view('user.request.block');
    }


    public function showDataTableBlock(Request $request)
    {
        $block = Block::with('user_blocked')->where('user_blocker_id', auth('user')->user()->id)->latest()->select('blocks.*');

        return datatables()->eloquent($block)
            ->editColumn('user_blocked.nick_name', function ($block) {
                return '<a href="' . route('user.show', $block->user_blocked) . '">' . $block->user_blocked->nick_name . '</a>';
            })
            ->addColumn('action', function (Block $block) {
                return '<a href="' . route('user.block.delete', $block) . '" class="text-danger">حذف از مسدود</a>';
            })
            ->rawColumns(['action' => 'action', 'user_blocked.nick_name' => 'user_blocked.nick_name'])
            ->make(true);
    }

    public function blockDelete(Block $block)
    {
        if ($block->user_blocker_id == auth('user')->user()->id) {
            $block->delete();
            return back()->with('success', trans('message.success_deleted'));
        }
        return back();
    }
}
