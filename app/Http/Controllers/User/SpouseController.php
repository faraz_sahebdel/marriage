<?php

namespace App\Http\Controllers\User;

use App\Components\MyKavenegar;
use App\Http\Controllers\Controller;
use App\Model\Block;
use App\Model\City;
use App\Model\Like;
use App\Model\Report;
use App\Model\Request as ModelRequest;
use App\Model\User;
use App\Model\Visit;
use Carbon\Carbon;
use DB;
use Hekmatinasser\Verta\Verta;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use MyFunctions;

class SpouseController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('user.auth');
    }

    public function index()
    {
        $cities = City::with('cities')
            ->where('parent_id', null)
            ->get();

        $age_ranges = User::AGE_RANGE_LIST;

        $marital_status_lists = User::MARITAL_STATUS_LIST;

        $height_range_lists = User::HEIGHT_RANGE_LIST;

        $weight_range_lists = User::WEIGHT_RANGE_LIST;

        $education_lists = User::EDUCATION_LIST;

        $residence_lists = User::RESIDENCE_STATUS_LIST;

        $employment_status_lists = User::EMPLOYMENT_STATUS_LIST;

        $religious_lists = User::RELIGIOUS_LIST;

        $dowry_lists = User::DOWRY_LIST;


        $data = [
            'cities',
            'age_ranges',
            'marital_status_lists',
            'height_range_lists',
            'weight_range_lists',
            'education_lists',

            'residence_lists',
            'employment_status_lists',
            'religious_lists',
            'dowry_lists',
        ];

        return view('user.spouse.index', compact($data));
    }

    public function showDataTable(Request $request)
    {
        // $users = User::get();
        $users = DB::table('users')
            ->whereNotNull('city_id')
            ->whereNotNull('age')
            ->whereNotNull('marital_status')
            ->whereNotNull('height')
            ->whereNotNull('weight')
            ->whereNotNull('education')
            ->whereNotNull('employment_status')
            ->whereNotNull('religious')
            ->whereNotNull('dowry')
            ->whereNotNull('dowry_cash')
            ->whereNotNull('child')
            ->whereNotNull('story')
            ->where('id', '!=', auth('user')->user()->id);

        if ($request->get('city_id') != 'all') {
            $users->where('city_id', $request->get('city_id'));
        }

        if ($request->get('age') != 'all') {
            if ($request->get('age') == USER::R18_25) {
                $users->whereBetween('age', [
                    verta::now()
                        ->subYear(25)
                        ->format('Y/m/d'),
                    verta::now()
                        ->subYear(18)
                        ->format('Y/m/d'),
                ]);
            } elseif ($request->get('age') == USER::R26_30) {
                $users->whereBetween('age', [
                    verta::now()
                        ->subYear(30)
                        ->format('Y/m/d'),
                    verta::now()
                        ->subYear(26)
                        ->format('Y/m/d'),
                ]);
            } elseif ($request->get('age') == USER::R31_35) {
                $users->whereBetween('age', [
                    verta::now()
                        ->subYear(35)
                        ->format('Y/m/d'),
                    verta::now()
                        ->subYear(31)
                        ->format('Y/m/d'),
                ]);
            } elseif ($request->get('age') == USER::R36_40) {
                $users->whereBetween('age', [
                    verta::now()
                        ->subYear(40)
                        ->format('Y/m/d'),
                    verta::now()
                        ->subYear(36)
                        ->format('Y/m/d'),
                ]);
            } elseif ($request->get('age') == USER::R41_50) {
                $users->whereBetween('age', [
                    verta::now()
                        ->subYear(50)
                        ->format('Y/m/d'),
                    verta::now()
                        ->subYear(41)
                        ->format('Y/m/d'),
                ]);
            } elseif ($request->get('age') == USER::R50_up) {
                $users->where(
                    'age',
                    '<',
                    verta::now()
                        ->subYear(50)
                        ->format('Y/m/d')
                );
            }
        }

        if ($request->get('marital_status') != 'all') {
            $users->where('marital_status', $request->get('marital_status'));
        }

        if ($request->get('height_status') != 'all') {
            if ($request->get('height_status') == USER::H150D) {
                $users->where('height', '<', '150');
            } elseif ($request->get('height_status') == USER::H150_175) {
                $users->whereBetween('height', ['150', '175']);
            } elseif ($request->get('height_status') == USER::H170U) {
                $users->where('height', '>', '175');
            }
        }

        if ($request->get('weight_status') != 'all') {
            if ($request->get('weight_status') == USER::W50D) {
                $users->where('weight', '<=', '50');
            } elseif ($request->get('weight_status') == USER::W51_60) {
                $users->whereBetween('weight', ['51', '60']);
            } elseif ($request->get('weight_status') == USER::W61_75) {
                $users->whereBetween('weight', ['61', '75']);
            } elseif ($request->get('weight_status') == USER::W76U) {
                $users->where('weight', '>=', '76');
            }
        }

        if ($request->get('education') != 'all') {
            $users->where('education', $request->get('education'));
        }

        if ($request->get('residence') != 'all') {
            $users->where('residence', $request->get('residence'));
        }

        if ($request->get('employment_status') != 'all') {
            $users->where('employment_status', $request->get('employment_status'));
        }

        if ($request->get('religious') != 'all') {
            $users->where('religious', $request->get('religious'));
        }

        if ($request->get('dowry') != 'all') {
            $users->where('dowry', $request->get('dowry'));
        }

        $users->get();

        return datatables()
            ->of($users)
            ->addColumn('action', function ($user) {
                return '<a href="' .
                    route('user.show', $user->id) .
                    '">مشاهده</a>';
            })
            ->editColumn('education', function ($user) {
                return User::EDUCATION_LIST[$user->education];
            })
            ->editColumn('residence', function ($user) {
                return User::RESIDENCE_STATUS_LIST[$user->residence];
            })
            ->editColumn('religious', function ($user) {
                return User::RELIGIOUS_LIST[$user->religious];
            })
            ->editColumn('dowry', function ($user) {
                return User::DOWRY_LIST[$user->dowry];
            })
            ->editColumn('marital_status', function ($user) {
                return User::MARITAL_STATUS_LIST[$user->marital_status];
            })
            ->editColumn('employment_status', function ($user) {
                return User::EMPLOYMENT_STATUS_LIST[$user->employment_status];
            })
            ->editColumn('age', function ($user) {
                $verta = Verta::parse($user->age);
                return $verta->diffYears();
            })
            ->rawColumns(['action' => 'action'])
            ->make(true);
    }

    public function userShow($id)
    {
        $user = User::findOrFail($id);

        $age = Verta::parse($user->age);
        $age = $age->diffYears();

        $last_online = new Verta($user->last_online);
        $last_online = $last_online->format('j F Y');

        $city_parent = City::findOrFail($user->city->parent_id);

        $model_request = ModelRequest::where('user_applicant_id', auth('user')->user()->id)
            ->where('user_acceptor_id', $user->id)->where('status', 0)->first();

        $visit = Visit::where('user_visitor_id', auth('user')->user()->id)
            ->where('user_visited_id', $user->id)->first();
        if (!$visit) {
            $visit = new Visit();
            $visit->user_visitor_id = auth('user')->user()->id;
            $visit->user_visited_id = $user->id;
            $visit->count = 1;
            $visit->save();
        } else {
            $visit->count = $visit->count + 1;
            $visit->save();
        }

        $like = false;
        if (Like::where('user_liker_id', auth('user')->user()->id)->where('user_liked_id', $user->id)->first()) {
            $like = true;
        }

        $reports = Report::REPORT_LIST;

        return view('user.spouse.show', compact('user', 'age', 'last_online', 'city_parent', 'model_request', 'visit', 'like', 'reports'));
    }

    public function userRequest(User $user)
    {
        if (MyFunctions::checkUserCompleteData()) {
            return back()->with('error', trans('message.error_data_incomplete'));
        }
        if ($user->gender != auth('user')->user()->gender && $user->id != auth('user')->user()->id) {

            if (ModelRequest::where('user_applicant_id', auth('user')->user()->id)->where('user_acceptor_id', $user->id)->first()) {
                return back()->with('warning', trans('message.warning_request'));
            }
            if (auth('user')->user()->sms_count > 0) {
                $request = new ModelRequest();
                $request->user_applicant_id = auth('user')->user()->id;
                $request->user_acceptor_id = $user->id;
                if ($request->save()) {
                    
                    //sms for user_acceptor_id
                    // MyKavenegar::instance()->lookup($order->user->mobile, null, null 'ezdsignup');
                    MyKavenegar::instance()->lookup($request->user_acceptor->mobile, 'درخواست جدید', null, null, 'ezdsignup');

                    return back()->with('success', trans('message.success_request'));
                }
            } else {
                return back()->with('warning', trans('message.warning_limit_sms_request'));
            }
        }
        return back()->with('error', trans('message.error_request'));
    }

    public function userRequestLike(Request $request)
    {
        if ($request->ajax()) {
            $like = Like::where('user_liker_id', auth('user')->user()->id)->where('user_liked_id', $request->user_id)->first();
            if (!$like) {
                $like = new Like();
                $like->user_liker_id = auth('user')->user()->id;
                $like->user_liked_id = $request->user_id;
                $like->save();
            } else {
                $like->delete();
            }
        }
    }

    public function userBlock(User $user)
    {
        if (!Block::where('user_blocker_id', auth('user')->user()->id)->where('user_blocked_id', $user->id)->first()) {
            $block = new Block();
            $block->user_blocker_id = auth('user')->user()->id;
            $block->user_blocked_id = $user->id;
            $block->save();
            return back()->with('success', trans('message.success_block'));
        } else {
            return back()->with('error', trans('message.error_block'));
        }
    }

    public function userReport(Request $request)
    {
        if (!Report::where('user_reporter_id', auth('user')->user()->id)->where('user_reported_id', $request->id)->first()) {
            $report = new Report();
            $report->user_reporter_id = auth('user')->user()->id;
            $report->user_reported_id = $request->id;
            $report->save();
            return back()->with('success', trans('message.success_report'));
        } else {
            return back()->with('error', trans('message.error_report'));
        }
    }
}
