<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Mail\AdminMail;
use App\Model\Admin;
use App\Model\Ticket;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Http\Request;
use Mail;

class TicketController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('user.auth');
    }


    public function index()
    {
        return view('user.ticket.index');
    }

    public function showDataTableTicket(Request $request)
    {
        $tikets = Ticket::where('user_id', auth('user')->user()->id)->where('parent_id', null)->latest();

        return datatables()->eloquent($tikets)
            ->editColumn('status', function ($tikets) {
                $result = '';
                if ($tikets->status == Ticket::STATUS_USER_RESPONSE) {
                    $result = '<span class="text-info">پاسخ شما</span>';
                } else if ($tikets->status == Ticket::STATUS_ADMIN_RESPONSE) {
                    $result = '<span class="text-warning">پاسخ مدیر</span>';
                } else if ($tikets->status == Ticket::STATUS_CLOSED_TICKET) {
                    $result = '<span class="text-success">بسته شده</span>';
                }
                return $result;
            })
            ->editColumn('created_at', function ($tikets) {
                return Verta($tikets->created_at)->format('Y/m/d H:i');
            })
            ->addColumn('action', function ($tikets) {
                return '<a href="' . route('user.ticket.send', $tikets) . '" class="text-success">پاسخ</a>';
            })
            ->rawColumns(['action' => 'action', 'status' => 'status'])
            ->make(true);
    }

    public function send(Ticket $ticket)
    {
        if (isset($ticket->id)) {
            $tickets = Ticket::where('parent_id', $ticket->id)->where('user_id', auth('user')->user()->id)->get();
            return view('user.ticket.send', compact('ticket', 'tickets'));
        }
        return view('user.ticket.send');
    }

    public function sendData(Request $request)
    {
        if ($request->ticket == null) {

            $ticket = new Ticket();
            $ticket->user_id = auth('user')->user()->id;
            $ticket->content = $request->message;
            $ticket->status = '0';
            if ($ticket->save()) {
                return redirect()->route('user.ticket.send', $ticket);
            }
        } elseif (Ticket::where('user_id', auth('user')->user()->id)->where('id', $request->ticket)->first()) {
            $ticket = new Ticket();
            $ticket->user_id = auth('user')->user()->id;
            $ticket->parent_id = $request->ticket;
            $ticket->content = $request->message;
            $ticket->status = '0';
            if ($ticket->save()) {
                Ticket::where('id', $request->ticket)->update(['status' => '0']);

                $details = [
                    'title' => 'جواب تیکت',
                    'body' => 'به تیکت شما جواب جدید ارسال شده است.'
                ];

                if ($ticket->admin != null) {
                    Mail::to($ticket->admin->email)->send(new AdminMail($details));
                }

                return back();
            }
        }
        return back();
    }
}
