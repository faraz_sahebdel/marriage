<?php

namespace App\Http\Controllers\User;

use App\Model\City;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserUpdateRequest;
use App\Mail\AdminMail;
use App\Model\Alert;
use App\Model\Config;
use App\Model\User;
use Auth;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use MyFunctions;
use Intervention\Image\Facades\Image;
use Mail;
use Session;

class UserController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('user.auth');
        $this->middleware('user.check.enable.code')->except(['index', 'enableCode']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        // $details = [
        //     'title' => 'پیام جدید است',
        //     'body' => 'سلام این یک پیام جدید است.'
        // ];

        // Mail::to('f_s_n.2012@yahoo.com')->send(new AdminMail($details));

        $alerts = Alert::all();

        return view('user.dashboard', compact('alerts'));
    }

    public function enableCode(Request $request)
    {
        $request->validate([
            'enable_code' => 'required'
        ]);

        if (auth()->guard('user')->user()->code == $request->enable_code) {
            User::where('id', auth()->guard('user')->user()->id)
                ->update(['enable_code' => true]);
            return back()->with('success', 'با موفقیت فعالسازی شد.');
        }

        return back()->with('error', 'کد فعالسازی اشتباه است.');
    }

    public function infoUpdateForm()
    {
        $cities = City::with('cities')->where('parent_id', null)->get();

        $user = auth('user')->user();

        return view('user.info', compact('user', 'cities'));
    }

    public function infoSave(UserUpdateRequest $request)
    {
        $user = User::findOrFail(auth('user')->user()->id);

        if ($request->sms_count) {
            return back();
        }

        $data = [];
        foreach($request->all() as $key => $value){
            $data[$key] = strip_tags($value);
        }

        if ($user->update($data)) {
            if (auth('user')->user()->mobile != $request->mobile) {
                $user->code = Str::random(5);
                $user->enable_code = false;
                $user->save();

                // TODO SMS
            }

            $request->session()->remove('warning');

            return redirect()->route('user.info.update')->with('success', trans('message.success_updated'));
        }

        return back()->with('warning', trans('message.error_update'));
    }

    public function setting()
    {
        return view('user.setting');
    }

    public function resetPassword(Request $request)
    {
        $request->validate([
            'old_password' => ['required'],
            'password' => ['required', 'confirmed', 'min:8'],
        ]);

        if (!Hash::check($request->old_password, auth()->guard('user')->user()->password)) {
            return back()->with('error', 'رمز عبور قبلی اشتباه است.');
        }

        $user = User::find(auth()->guard('user')->user()->id);

        $user->password = Hash::make($request->password);
        if ($user->save()) {

            auth('user')->logout();

            return redirect()->route('user.login.form');
        }

        return back();
    }
}
