<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class ChangeDefaultDriver
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (auth('user')->check()) {
            Auth::setDefaultDriver('user');
        } else if (auth('admin')->check()) {
            Auth::setDefaultDriver('admin');
        } else if (auth('office')->check()) {
            Auth::setDefaultDriver('admin');
        }
        return $next($request);
    }
}
