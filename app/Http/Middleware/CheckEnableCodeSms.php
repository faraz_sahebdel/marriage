<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class CheckEnableCodeSms
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('user')->user()->enable_code == false) {
            return redirect()->route('user.index');
        }
        return $next($request);
    }
}
