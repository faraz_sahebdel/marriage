<?php

namespace App\Http\Middleware;

use Closure;

class OfficeAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!auth('office')->check()) {
            return redirect()->route('office.login.form');
        }
        
        return $next($request);
    }
}
