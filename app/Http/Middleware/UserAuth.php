<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use DB;
use Illuminate\Support\Facades\Auth;
use MyFunctions;
use Route;

class UserAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!auth('user')->check()) {
            return redirect()->route('user.login.form');
        }

        DB::table('users')->where('id', auth('user')->user()->id)->update(['last_online' => Carbon::now()]);

        if (MyFunctions::checkUserCompleteData()) {
            $message = "اطلاعات شما کامل نیست و برای ادامه کار باید اطلاعات خود را تکمیل کنید " . "<a href='" . route('user.info.update') . "'>" . "جهت تکمیل اطلاعات کلیک کنید. " . "</a>";
            session()->flash('warning', $message);
        }

        if (auth('user')->user()->block_admin) {
            $request->session()->flash('error', trans('message.error_admin_banned'));
            if ($request->route()->getName() != 'user.index') {
                return redirect()->route('user.index');
            }
        }
        return $next($request);
    }
}
