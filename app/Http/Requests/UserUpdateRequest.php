<?php

namespace App\Http\Requests;

use App\Model\User;
use App\Rules\IsAdminRequired;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // $this->route('id')
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user_id = null;
        if (auth('user')->check()) {
            $user_id = auth('user')->user()->id;
        }

        return [
            'nick_name' => [
                'max:255',
                new IsAdminRequired,
            ],
            'mobile' => [
                'min:11',
                'max:11',
                new IsAdminRequired,
                Rule::unique('users')->ignore($user_id, 'id'),
            ],
            'image' => [
                'mimes:jpeg,bmp,png,jpg|max:2048',
            ],
            'city_id' => [
                new IsAdminRequired,
                Rule::exists('cities', 'id')->where(function ($query) {
                    $query->whereNotNull('parent_id');
                }),
            ],
            'age' => [
                new IsAdminRequired,
                'regex:/^1[34][0-9][0-9]\/((0[1-6]\/((3[0-1])|([1-2][0-9])|(0[1-9])))|((1[0-2]|(0[7-9]))\/(30|31|([1-2][0-9])|(0[1-9]))))$/i',
            ],
            'marital_status' => [
                new IsAdminRequired,
                Rule::in(array_keys(User::MARITAL_STATUS_LIST))
            ],
            'height' => [
                'max:3',
                new IsAdminRequired,
            ],
            'weight' => [
                'max:3',
                new IsAdminRequired,
            ],
            'education' => [
                new IsAdminRequired,
                Rule::in(array_keys(User::EDUCATION_LIST))
            ],
            'residence' => [
                new IsAdminRequired,
                Rule::in(array_keys(User::RESIDENCE_STATUS_LIST))
            ],
            'employment_status' => [
                new IsAdminRequired,
                Rule::in(array_keys(User::EMPLOYMENT_STATUS_LIST))
            ],
            'religious' => [
                new IsAdminRequired,
                Rule::in(array_keys(User::RELIGIOUS_LIST))
            ],
            'dowry' => [
                new IsAdminRequired,
                Rule::in(array_keys(User::DOWRY_LIST))
            ],
            'gender' => [
                new IsAdminRequired,
            ],
            'dowry_cash' => [
                new IsAdminRequired,
                'numeric',
            ],
            'child' => [
                new IsAdminRequired,
                'numeric',
            ],
            'story' => [
                new IsAdminRequired,
            ],
        ];
    }
}
