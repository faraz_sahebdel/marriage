<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Alert extends Model
{
    
    use LogsActivity;

    protected static $logAttributes = ['*'];

}
