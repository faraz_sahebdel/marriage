<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Block extends Model
{

    use LogsActivity;

    protected static $logAttributes = ['*'];
    
    public function user_blocker()
    {
        return $this->hasOne('App\Mode\l\User', 'id', 'user_blocker_id');
    }

    public function user_blocked()
    {
        return $this->hasOne('App\Model\User', 'id', 'user_blocked_id');
    }
}
