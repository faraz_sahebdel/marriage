<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{

    public function cities()
    {
        return $this->hasMany('App\Model\City', 'parent_id', 'id');
    }
}
