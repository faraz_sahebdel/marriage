<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Config extends Model
{

    use LogsActivity;

    protected static $logAttributes = ['*'];

    protected $fillable = [
        'free_sms'
    ];
}
