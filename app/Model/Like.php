<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Like extends Model
{

    use LogsActivity;

    protected static $logAttributes = ['*'];

    
    protected $fillable = [
        'user_liker_id',
        'user_liked_id'
    ];

    public function user()
    {
        return $this->hasOne('App\Model\User', 'id', 'user_liked_id');
    }
}