<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Activitylog\Traits\LogsActivity;

class Office extends Authenticatable
{

    use LogsActivity;

    protected static $logAttributes = ['*'];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $fillable = [
        'city_id',
        'name',
        'first_name',
        'last_name',
        'mobile',
        'phone',
        'address',
        'password',
    ];

    public function city()
    {
        return $this->belongsTo('App\Model\City');
    }
}
