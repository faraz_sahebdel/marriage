<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Order extends Model
{
    use SoftDeletes, LogsActivity;

    protected static $logAttributes = ['*'];
    
    protected $dates = ['deleted_at'];

    public const STATUS_WATING = 0;
    public const STATUS_FAILED = 1;
    public const STATUS_SUCCESS = 2;

    const STATUS_ORDER_LIST = [
        self::STATUS_WATING => 'پرداخت در حالت انتظار',
        self::STATUS_FAILED => 'پرداخت ناموفق',
        self::STATUS_SUCCESS => 'پرداخت موفق',
    ];

    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }

    public function package()
    {
        return $this->belongsTo('App\Model\Package');
    }
}
