<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Package extends Model
{
    use SoftDeletes, LogsActivity;

    protected static $logAttributes = ['*'];
    
    protected $fillable = [
        'price',
        'count',
    ];

}
