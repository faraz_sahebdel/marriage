<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Report extends Model
{

    use LogsActivity;

    protected static $logAttributes = ['*'];


    public const SUSPICIOUS_USER = 0;
    public const MISLEADING_INFORMATION = 1;
    public const ANNOYING_USER = 2;
    public const OTHER_CASES = 3;

    const REPORT_LIST = [
        self::SUSPICIOUS_USER => 'این کاربر مشکوک است',
        self::MISLEADING_INFORMATION => 'این کاربر اطلاعات گمراه کننده ای ارسال کرده',
        self::ANNOYING_USER => 'این کاربر مزاحم من است',
        self::OTHER_CASES => 'سایر موارد',
    ];

    public function user_reporter()
    {
        return $this->hasOne('App\Model\User', 'id', 'user_reporter_id');
    }

    public function user_report()
    {
        return $this->hasOne('App\Model\User', 'id', 'user_reported_id');
    }
}
