<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Request extends Model
{
    
    use LogsActivity;

    protected static $logAttributes = ['*'];


    public function user()
    {
        return $this->hasOne('App\Model\User', 'id', 'user_applicant_id');
    }

    public function user_acceptor()
    {
        return $this->hasOne('App\Model\User', 'id', 'user_acceptor_id');
    }

    public function office()
    {
        return $this->hasOne('App\Model\Office', 'id', 'office_id');
    }
    
}
