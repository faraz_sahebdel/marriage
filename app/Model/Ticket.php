<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Ticket extends Model
{
    use LogsActivity;

    protected static $logAttributes = ['*'];

    public const STATUS_USER_RESPONSE  = 0;
    public const STATUS_ADMIN_RESPONSE = 1;
    public const STATUS_CLOSED_TICKET  = 2;


    public function user()
    {
        return $this->hasOne('App\Model\User', 'id', 'user_id');
    }

    public function admin()
    {
        return $this->hasOne('App\Model\Admin', 'id', 'admin_id');
    }

    public function parent()
    {
        return $this->hasOne('App\Model\Ticket', 'id', 'parent_id');
    }
}
