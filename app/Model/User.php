<?php

namespace App\Model;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Spatie\Activitylog\Traits\LogsActivity;

class User extends Authenticatable
{
    use Notifiable, LogsActivity;

    protected static $logAttributes = ['*'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nick_name',
        'mobile',
        'image',
        'code',
        'password',
        'gender',
        'city_id',
        'age',
        'marital_status',
        'height',
        'weight',
        'education',
        'residence',
        'employment_status',
        'religious',
        'dowry',
        'dowry_cash',
        'child',
        'story',
        'sms_count',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public const SINGLE = 0;
    public const DIVORCED = 1;
    public const WIFE_OF_THE_DECEASED = 2;
    public const MARRIED = 3;

    const MARITAL_STATUS_LIST = [
        self::SINGLE => 'مجرد',
        self::DIVORCED => 'مطلقه',
        self::WIFE_OF_THE_DECEASED => 'همسر مرحوم',
        self::MARRIED => 'متاهل',
    ];

    public const CYCLE = 0;
    public const DIPLOMA = 1;
    public const ASSOCIATE = 2;
    public const EXPERT = 3;
    public const MASTERS = 4;
    public const DOCTORATE = 5;
    public const DOCTOR = 6;

    const EDUCATION_LIST = [
        self::CYCLE => 'سیکل',
        self::DIPLOMA => 'دیپلم',
        self::ASSOCIATE => 'کاردانی',
        self::EXPERT => 'کارشناسی',
        self::MASTERS => 'کارشناسی ارشد',
        self::DOCTORATE => 'دکترا (PHD)',
        self::DOCTOR => 'پزشک',
    ];

    public const WITH_FAMILY = 0;
    public const JUST = 1;
    public const TOGETHER_HOME = 2;
    public const DORM = 3;

    const RESIDENCE_STATUS_LIST = [
        self::WITH_FAMILY => 'همراه با خانواده',
        self::JUST => 'مستقل مجردی',
        self::TOGETHER_HOME => 'همراه با هم خانه',
        self::DORM => 'خوابگاه',
    ];

    public const UNEMPLOYED = 0;
    public const HOUSEWIFE = 1;
    public const PART_TIME_EMPLOYEE = 2;
    public const WORK_FULL_TIME = 3;

    const EMPLOYMENT_STATUS_LIST = [
        self::UNEMPLOYED => 'بیکار',
        self::HOUSEWIFE => 'خانه دار',
        self::PART_TIME_EMPLOYEE => 'شاغل پاره وقت',
        self::WORK_FULL_TIME => 'شاغل تمام وقت',
    ];


    public const MODERATE_RELIGION = 0;
    public const BELIEVER = 1;
    public const NON_RELIGIOUS = 2;

    const RELIGIOUS_LIST = [
        self::MODERATE_RELIGION => 'معتدل',
        self::BELIEVER => 'معتقد و مقید',
        self::NON_RELIGIOUS => 'غیر مذهبی',
    ];

    public const DAILY = 0;
    public const WEEKLY = 1;
    public const MONTHLY = 2;
    public const YEARLY = 3;

    const DOWRY_LIST = [
        self::DAILY => 'روزانه',
        self::WEEKLY => 'هفتگی',
        self::MONTHLY => 'ماهانه',
        self::YEARLY => 'سالانه',
    ];

    public const R18_25 = 0;
    public const R26_30 = 1;
    public const R31_35 = 2;
    public const R36_40 = 3;
    public const R41_50 = 4;
    public const R50_up = 5;

    const AGE_RANGE_LIST = [
        self::R18_25 => '18 تا 25',
        self::R26_30 => '26 تا 30',
        self::R31_35 => '31 تا 35',
        self::R36_40 => '36 تا 40',
        self::R41_50 => '41 تا 50',
        self::R50_up => '50 سال به بالا',
    ];

    public const H150D = 0;
    public const H150_175 = 1;
    public const H170U = 2;

    const HEIGHT_RANGE_LIST = [
        self::H150D => 'کوتاه = زیر 150 سانتی متر',
        self::H150_175 => 'معمولی = 150 تا 175 سانتی متر',
        self::H170U => 'بلند 176 به بالا',
    ];


    public const W50D = 0;
    public const W51_60 = 1;
    public const W61_75 = 2;
    public const W76U = 3;

    const WEIGHT_RANGE_LIST = [
        self::W50D => 'زیر 50 کیلوگرم',
        self::W51_60 => '51 الی 60 کیلوگرم',
        self::W61_75 => '61 الی 75 کیلوگرم',
        self::W76U => '76 کیلو به بالا',
    ];

    public function city()
    {
        return $this->belongsTo('App\Model\City');
    }

    private $_oldImage = NULL;

    public function setImageAttribute($value)
    {
        // if (is_null($value)) return;
        // $name = Str::uuid() . '.' . $value->extension();
        // $path = public_path('uploads') . '/' . $name;
        // Image::make($value)->save($path);
        // $this->_oldImage = $this->image;
        // $this->attributes['image'] = $name;

        // $fileOldImagePath = public_path('uploads') . '/' . $this->_oldImage;
        // if ($this->_oldImage != null && file_exists($fileOldImagePath)) {
        //     unlink($fileOldImagePath);
        // }
    }
}
