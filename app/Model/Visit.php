<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Visit extends Model
{
    
    use LogsActivity;

    protected static $logAttributes = ['*'];

}
