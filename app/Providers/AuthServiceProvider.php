<?php

namespace App\Providers;

use App\Model\Admin;
use App\Model\Permission;
use App\Model\Permissions;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Schema;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        if (Schema::hasTable('permissions')) {

            foreach (Permission::all() as $permission) {
                Gate::define($permission->name, function ($admin) use ($permission) {
                    return $admin->permissions->contains('name', $permission->name);
                });
            }
        };
    }
}
