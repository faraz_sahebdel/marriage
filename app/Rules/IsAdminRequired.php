<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Validation\Rule as ValidationRule;

class IsAdminRequired implements Rule
{
    private $user_id;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($user_id = null)
    {
        $this->user_id = $user_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // if ($attribute == 'mobile') {
        //     if (auth('admin')->check())
        //         return ValidationRule::unique('users', 'mobile')->ignore($this->user_id, 'id');
        //     else if (auth('user')->check()) {
        //         return ValidationRule::unique('users')->ignore(auth('user')->user()->id, 'id');
        //     }
        // }

        if ($value == null) {
            if (auth('admin')->check())
                return true;
            else if (auth('user')->check())
                return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.required');
    }
}
