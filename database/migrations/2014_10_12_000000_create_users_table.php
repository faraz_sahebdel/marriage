<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('nick_name');
            $table->string('mobile', 11)->unique();
            $table->string('image')->nullable();
            $table->string('code', 5)->nullable();
            $table->boolean('enable_code')->default(false);
            $table->timestamp('mobile_verified_at')->nullable();
            $table->string('password');
            $table->enum('gender', [0, 1])->comment('0 => female, 1 => man');
            $table->string('mobile_tmp', 11)->nullable();
            $table->boolean('block_admin')->default(0)->comment('0 => not block, 1 => blocked');
            $table->unsignedBigInteger('city_id')->nullable();
            $table->string('age', 10)->nullable();
            $table->enum('marital_status', [0, 1, 2, 3])->comment('0 => single, 1 => divorced, 2 => wife of the deceased, 3 => married')->nullable();
            $table->string('height', 3)->nullable();
            $table->string('weight', 3)->nullable();
            $table->enum('education', [0, 1, 2, 3, 4, 5, 6])->comment('0 => cycle, 1 => diploma, 2 => associate, 3 => expert, 4 => masters, 5 => doctorate, 6 => doctor')->nullable();
            $table->enum('residence', [0, 1, 2, 3])->comment('0 => With family, 1 => Just, 2 => Together home, 3 => Dorm')->nullable();
            $table->enum('employment_status', [0, 1, 2, 3])->comment('0 => unemployed, 1 => housewife, 2 => part-time employee, 3 => work full time')->nullable();
            $table->enum('religious', [0, 1, 2])->comment('0 => moderate religion, 1 => believer, 2 => non-religious')->nullable();
            $table->enum('dowry', [0, 1, 2, 3])->comment('0 => daily, 1 => weekly, 2 => monthly, 3 => yearly')->nullable();
            $table->string('dowry_cash', 255)->nullable();
            $table->boolean('child')->default(0);
            $table->text('story')->nullable();
            $table->string('sms_count', 10)->nullable();
            $table->timestamp('last_online')->nullable();
            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
