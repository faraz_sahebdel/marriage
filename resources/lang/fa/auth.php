 
<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'مشخصات وارد شده با اطلاعات ما سازگار نیست.',
    'throttle' => 'دفعات تلاش شما برای ورود بیش از حد مجاز است. لطفا پس از :seconds ثانیه مجددا تلاش فرمایید.',

    'email' => 'ایمیل',
    'nick_name' => 'اسم مستعاره',
    'password' => 'رمز عبور',
    'repeat_password' => 'تکرار رمز عبور',
    'mobile' => 'تلفن همراه',
    'gender' => 'جنسیت',
    'register' => 'ثبت نام',
    'token' => 'رمز یکبار مصرف',

    'first_name' => 'نام',
    'last_name' => 'نام خانوادگی',
    
];
