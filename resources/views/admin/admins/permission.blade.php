@extends('layouts.admin')

@section('title','مجوز')

@section('content')

<div class="row">

    <div class="col-md-6">
        <div class="card">

            <div class="card-header header-elements-inline">
                <h5 class="card-title">تخصیص مجوز به مدیر: {{ $admin->name }}</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a href="{{ route('admin.admins.index') }}"><button class="list-icons-item text-danger"><i
                                    class="icon-arrow-left12 icon-2x"></i></button></a>
                    </div>
                </div>
            </div>


            <div class="card-body">
@can('user-insert')
    
<p>adsdddddddddddddddas</p>
@endcan
                <form action="{{ route('admin.admins.permission', $admin) }}" method="POST">

                    @csrf

                    <table class="table tab-content-bordered">
                        <thead>
                            <tr>
                                <td>مجوزر</td>
                                <td><i class="icon-gear"></i></td>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($permissions as $permission)
                            <tr>
                                <td>{{ $permission->text }}</td>
                                <td>
                                    <input type="checkbox" name="permissions[]" value="{{ $permission->id }}" {{ ($admin->permissions->contains('name', $permission->name) ? 'checked' : '') }}>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>



                    <div class="text-right">
                        <button type="submit" class="btn btn-success">ذخیره
                            <i class="icon-paperplane ml-2"></i></button>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>
@endsection