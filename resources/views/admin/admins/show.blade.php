@extends('layouts.admin')

@section('title','')

@section('content')

<div class="row">

    <div class="col-md-6">
        <div class="card">

            <div class="card-header header-elements-inline">
                <h5 class="card-title">مدیر: {{ $admin->name }}</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <form action="{{ route('admin.admins.destroy', $admin) }}"
                            onsubmit="return confirm('{{ trans('message.confirm_remove_admin') }}')" method="POST">
                            @csrf
                            @method('delete')
                            <button class="list-icons-item text-danger"><i class="icon-cross icon-2x"></i></button>
                        </form>
                    <a href="{{ route('admin.admins.index') }}"><button class="list-icons-item text-danger"><i class="icon-arrow-left12 icon-2x"></i></button></a>
                    </div>
                </div>
            </div>


            <div class="card-body">


                <form action="{{ route('admin.admins.update', $admin) }}" method="POST" enctype="multipart/form-data">

                    @csrf

                    @method('PUT')

                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">نام</label>
                        <div class="col-lg-9">
                            <input type="text" name="name" class="form-control" value="{{ $admin->name }}">
                            @error('name')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">ایمیل</label>
                        <div class="col-lg-9">
                            <input type="text" name="email" class="form-control" value="{{ $admin->email }}">
                            @error('email')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>


                    <div class="text-right">

                        {{-- <a href="{{ route('admin.admins.permission', $admin) }}" class="btn btn-info">مجوز ها
                            <i class="icon-accessibility ml-2"></i></a> --}}

                            <button type="submit" class="btn btn-success">ذخیره
                                <i class="icon-paperplane ml-2"></i></button>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>
@endsection