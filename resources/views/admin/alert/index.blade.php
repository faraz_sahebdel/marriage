@extends('layouts.admin')

@section('title','اعلان ها')

@section('content')

<!-- Task manager table -->
<div class="card p-1">
    <div class="card-header bg-transparent header-elements-inline mb-1">
    <h6 class="card-title">اعلان ها <a href="{{ route('admin.alert.create') }}"><i class="icon-plus2 icon-2x text-success"></i></a></h6>
    </div>

    <table class="table tasks-list table-lg" id="laravel_datatable">
        <thead>
            <tr>
                <th>شماره</th>
                <th>متن</th>
                <th><i class="icon-gear"></i></th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>
<!-- /task manager table -->

@endsection

@push('script')

<script src="/js/data_table/jquery.dataTables.min.js"></script>

<script>
    $(document).ready( function () {
            $('#laravel_datatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('admin.alert.list') }}",
                language: { "url": "/json/persian.json" },
                order: [1,'asc'],
                pageLength: 15,
                columns: [
                            { data: 'id', name: 'id' },
                            { data: 'content', name: 'content' },
                            { data: 'action', name: 'action' },
                        ]
                });
            });
</script>
@endpush