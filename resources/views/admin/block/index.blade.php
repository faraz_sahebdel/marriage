@extends('layouts.admin')

@section('title','مسدود شده توسط کاربران')

@section('content')

<!-- Task manager table -->
<div class="card p-1">
    <div class="card-header bg-transparent header-elements-inline mb-1">
    <h6 class="card-title">مسدود شده توسط کاربران</h6>
    </div>

    <table class="table tasks-list table-lg" id="laravel_datatable">
        <thead>
            <tr>
                <th>شماره مسدود کننده</th>
                <th>مسدود کننده</th>
                <th>شماره مسدود شده</th>
                <th>مسدود شده</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>
<!-- /task manager table -->

@endsection

@push('script')

<script src="/js/data_table/jquery.dataTables.min.js"></script>

<script>
    $(document).ready( function () {
            $('#laravel_datatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('admin.block.list') }}",
                language: { "url": "/json/persian.json" },
                order: [1,'asc'],
                pageLength: 15,
                columns: [
                            { data: 'user_blocker_id', name: 'user_blocker_id' },
                            { data: 'user_blocker.nick_name', name: 'user_blocker.nick_name' },
                            { data: 'user_blocked_id', name: 'user_blocked_id' },
                            { data: 'user_blocked.nick_name', name: 'user_blocked.nick_name' },
                        ]
                });
            });
</script>
@endpush