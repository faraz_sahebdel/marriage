@extends('layouts.admin')

@section('title','تنظیمات سایت')

@section('content')

<!-- Main charts -->
<div class="row">

    <div class="col-md-6">

        <div class="card">

            <div class="card-header header-elements-inline">
                <legend>
                    <h5 class="card-title">تنظیمات سایت</h5>
                </legend>
            </div>
            <div class="card-body">

                <form action="{{ route('admin.config.update') }}" method="POST">

                    @csrf

                    <fieldset class="mb-3">

                        <div class="form-group row">
                            <label class="col-form-label col-lg-4">تعداد پیامک اولیه رایگان</label>
                            <div class="col-lg-8">
                            <input type="text" name="free_sms" class="form-control" value="{{ $config->free_sms }}">
                                @error('free_sms')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>


                    </fieldset>
                    <div class="text-right">
                        <button type="submit" class="btn btn-success">ذخیره <i
                                class="icon-paperplane ml-2"></i></button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

<!-- /main charts -->

@endsection