@extends('layouts.admin')

@section('title','داشبورد')

@section('content')

<!-- Main charts -->
<div class="row">
    <div class="col-sm-6 col-xl-3">
        <div class="card card-body">
            <div class="media">
                <div class="mr-3 align-self-center">
                    <i class="icon-pointer icon-3x text-success-400"></i>
                </div>

                <div class="media-body text-right">
                    <h3 class="font-weight-semibold mb-0">{{ App\Model\User::count() }}</h3>
                    <span class="text-uppercase font-size-sm text-muted">تعداد کاربران</span>
                </div>
            </div>
        </div>
    </div>


    <div class="card border-info mb-3">
        <div class="card-header text-center">اطلاعات پنل پیامک</div>
        <div class="card-body text-info">
            @if($data['info'])
            <p class="card-text">اعتبار باقی مانده:
                {{ MyFunctions::convertToPersianNumber($data['info']->remaincredit, true) }} ریال</p>
            @php
            $v = new Verta($data['info']->expiredate);
            $diff = $v->formatDifference();
            @endphp
            <p class="card-text">تاریخ انقضاء: {{ $diff }}</p>
            @else
                <p>ارتباط با کاوه نگار قطع است.</p>
            @endif
        </div>
    </div>



    <div class="col-sm-6 col-xl-3">
        <div class="card card-body">
            <div class="media">
                <div class="media-body">
                    <h3 class="font-weight-semibold mb-0">{{ App\Model\User::where('enable_code', null)->count() }}</h3>
                    <span class="text-uppercase font-size-sm text-muted">کاربران منتظر تائید</span>
                </div>

                <div class="ml-3 align-self-center">
                    <i class="icon-bubbles4 icon-3x text-blue-400"></i>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-6 col-xl-3">
        <div class="card card-body">
            <div class="media">
                <div class="media-body">
                    <h3 class="font-weight-semibold mb-0">389,438</h3>
                    <span class="text-uppercase font-size-sm text-muted">گزارشات منتظر تائید</span>
                </div>

                <div class="ml-3 align-self-center">
                    <i class="icon-bag icon-3x text-danger-400"></i>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6 col-xl-3">
        <div class="card card-body bg-blue-400 has-bg-image">
            <div class="media">
                <div class="media-body">
                    <h3 class="mb-0">54,390</h3>
                    <span class="text-uppercase font-size-xs">اعتبار پیامک</span>
                </div>

                <div class="ml-3 align-self-center">
                    <i class="icon-bubbles4 icon-3x opacity-75"></i>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-6 col-xl-3">
        <div class="card card-body bg-danger-400 has-bg-image">
            <div class="media">
                <div class="media-body">
                    <h3 class="mb-0">389,438</h3>
                    <span class="text-uppercase font-size-xs">واریزی ها امروز</span>
                </div>

                <div class="ml-3 align-self-center">
                    <i class="icon-bag icon-3x opacity-75"></i>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-6 col-xl-3">
        <div class="card card-body bg-success-400 has-bg-image">
            <div class="media">
                <div class="mr-3 align-self-center">
                    <i class="icon-pointer icon-3x opacity-75"></i>
                </div>

                <div class="media-body text-right">
                    <h3 class="mb-0">652,549</h3>
                    <span class="text-uppercase font-size-xs">کل واریزی</span>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-6 col-xl-3">
        <div class="card card-body bg-indigo-400 has-bg-image">
            <div class="media">
                <div class="mr-3 align-self-center">
                    <i class="icon-enter6 icon-3x opacity-75"></i>
                </div>

                <div class="media-body text-right">
                    <h3 class="mb-0">245,382</h3>
                    <span class="text-uppercase font-size-xs">کل پرداختی ها</span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /main charts -->

@endsection