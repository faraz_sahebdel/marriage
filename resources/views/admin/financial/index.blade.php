@extends('layouts.admin')

@section('title','داشبورد')

@section('content')

<!-- Main charts -->
<div class="row">

    <div class="col-sm-6 col-xl-3">
        <div class="card card-body">
            <div class="media">
                <div class="mr-3 align-self-center">
                    <i class="icon-pointer icon-3x text-success-400"></i>
                </div>

                <div class="media-body text-right">
                    <h3 class="font-weight-semibold mb-0">فروش کل</h3>
                <span class="text-uppercase font-size-sm text-muted">
                    {{ MyFunctions::convertToPersianNumber($data['all'], true) }} تومان</p>
                </span>
                </div>
            </div>
        </div>
    </div>


    <div class="col-sm-6 col-xl-3">
        <div class="card card-body">
            <div class="media">
                <div class="mr-3 align-self-center">
                    <i class="icon-pointer icon-3x text-success-400"></i>
                </div>

                <div class="media-body text-right">
                    <h3 class="font-weight-semibold mb-0">فروش ماهانه</h3>
                <span class="text-uppercase font-size-sm text-muted">
                    {{ MyFunctions::convertToPersianNumber($data['month'], true) }} تومان</p>
                </span>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-6 col-xl-3">
        <div class="card card-body">
            <div class="media">
                <div class="mr-3 align-self-center">
                    <i class="icon-pointer icon-3x text-success-400"></i>
                </div>

                <div class="media-body text-right">
                    <h3 class="font-weight-semibold mb-0">تعداد خرید موفق</h3>
                <span class="text-uppercase font-size-sm text-muted">
                    {{ MyFunctions::convertToPersianNumber($data['success'], true) }}</p>
                </span>
                </div>
            </div>
        </div>
    </div>

    

</div>
<!-- /main charts -->

@endsection