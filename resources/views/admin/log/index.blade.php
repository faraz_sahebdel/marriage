@extends('layouts.admin')

@section('title','')

@section('content')

<!-- Task manager table -->
<div class="card p-1">
    <div class="card-header bg-transparent header-elements-inline mb-1">
        <h6 class="card-title">لاگ ها <a href="{{ route('admin.log.export') }}"><i class="icon-file-excel icon-2x text-success"></i></a></h6>
        
    </div>

    <table class="table tasks-list table-lg" id="laravel_datatable">
        <thead>
            <tr>
                <th>شماره</th>
                <th>نام لاگ</th>
                <th>نوع</th>
                <th>نام کاربر یا موضوع انجام شده</th>
                <th>نام کاربر انجام دهنده</th>
                <th>تاریخ</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>
<!-- /task manager table -->

@endsection

@push('script')

<script src="/js/data_table/jquery.dataTables.min.js"></script>

<script>
    $(document).ready( function () {
            $('#laravel_datatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('admin.log.list') }}",
                language: { "url": "/json/persian.json" },
                order: [1,'asc'],
                pageLength: 15,
                columns: [
                            { data: 'id', name: 'id' },
                            { data: 'log_name', name: 'log_name' },
                            { data: 'description', name: 'description' },
                            { data: 'subject_id', name: 'subject_id' },
                            { data: 'causer_id', name: 'causer_id' },
                            { data: 'created_at', name: 'created_at' },
                        ]
                });
            });
</script>
@endpush