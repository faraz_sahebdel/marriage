@extends('layouts.admin')

@section('title','')

@section('content')

<!-- Task manager table -->
<div class="card p-1">
    <div class="card-header bg-transparent header-elements-inline mb-1">
        <h6 class="card-title"></h6>
        {{-- <a href="{{ route('admin.office.create') }}"><i class="icon-plus2 icon-2x text-success"></i></a> --}}
    </div>

    <table class="table tasks-list table-lg" id="laravel_datatable">
        <thead>
            <tr>
                <th>شماره</th>
                <th>شهر</th>
                <th>نام دفتر</th>
                <th>موبایل</th>
                <th>تلفن</th>
                <th>وضعیت</th>
                <th class="text-muted"><i class="icon-gear"></i></th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>
<!-- /task manager table -->

@endsection

@push('script')

<script src="/js/data_table/jquery.dataTables.min.js"></script>

<script>
    $(document).ready( function () {
            $('#laravel_datatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('admin.office.list') }}",
                language: { "url": "/json/persian.json" },
                order: [1,'asc'],
                pageLength: 15,
                columns: [
                            { data: 'id', name: 'id' },
                            { data: 'city.name', name: 'city.name' },
                            { data: 'name', name: 'name' },
                            { data: 'mobile', name: 'mobile' },
                            { data: 'phone', name: 'phone' },
                            { data: 'confirmed_admin', name: 'confirmed_admin' },
                            { data: 'action', name: 'action', orderable: false, searchable: false},
                        ]
                });
            });
</script>
@endpush