@extends('layouts.admin')

@section('title','')

@section('content')

<div class="row">

    <div class="col-md-6">
        <div class="card">

            <div class="card-header header-elements-inline">
                <h5 class="card-title">کاربر: {{ $office->name }}</h5>
                <div class="header-elements">
                    <div class="list-icons">

                        <form action="{{ route('admin.office.destroy', $office) }}"
                            onsubmit="return confirm('آیا می خواهید دفتر مورد نظر حذف شود؟')" method="POST">
                            @csrf
                            @method('delete')
                            <button class="list-icons-item text-danger"><i class="icon-cross icon-2x"></i></button>
                        </form>
                        <a href="{{ route('admin.office.index') }}"><button class="list-icons-item text-danger"><i
                                    class="icon-arrow-left12 icon-2x"></i></button></a>
                    </div>
                </div>
            </div>


            <div class="card-body">


                <form action="{{ route('admin.office.update', $office) }}" method="POST" enctype="multipart/form-data">

                    @csrf

                    @method('PUT')

                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">اسم</label>
                        <div class="col-lg-9">
                            <input type="text" name="name" class="form-control" value="{{ $office->name }}">
                            @error('name')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">نام</label>
                        <div class="col-lg-9">
                            <input type="text" name="first_name" class="form-control" value="{{ $office->first_name }}">
                            @error('first_name')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">نام خانوادگی</label>
                        <div class="col-lg-9">
                            <input type="text" name="last_name" class="form-control" value="{{ $office->last_name }}">
                            @error('last_name')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">تلفن</label>
                        <div class="col-lg-9">
                            <input type="text" name="phone" class="form-control" value="{{ $office->phone }}">
                            @error('phone')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">تلفن همراه</label>
                        <div class="col-lg-9">
                            <input type="text" name="mobile" class="form-control" value="{{ $office->mobile }}">
                            @error('mobile')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>


                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">شهر زندگی</label>
                        <div class="col-lg-9">
                            <select name="city_id" class="form-control">
                                @foreach ($cities as $city)
                                <optgroup label="{{ $city->name }}">
                                    @foreach ($city->cities as $c)
                                    <option value="{{ $c->id }}" {{ ($office->city_id == $c->id ? 'selected' : '')}}>
                                        {{ $c->name }}
                                    </option>
                                    @endforeach
                                </optgroup>
                                @endforeach
                            </select>
                            @error('city_id')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>



                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">آدرس</label>
                        <div class="col-lg-9">
                            <textarea type="text" name="address" class="form-control"
                                rows="6">{{ $office->address }}</textarea>
                            @error('address')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">تایید دفتر</label>

                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="confirmed_admin" value="0"
                                    {{ ($office->confirmed_admin == 0 ? 'checked' : '') }}>
                                تایید نشده
                            </label>
                        </div>

                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="confirmed_admin" value="1"
                                    {{ ($office->confirmed_admin == 1 ? 'checked' : '') }}>
                                تایید شده
                            </label>
                        </div>
                    </div>

                    <div class="text-right">
                        <button type="submit" class="btn btn-success">ذخیره
                            <i class="icon-paperplane ml-2"></i></button>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>
@endsection