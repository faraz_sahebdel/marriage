@extends('layouts.admin')

@section('title','ایجاد بسته پیامکی')

@section('content')

<div class="row">

    <div class="col-md-6">
        <div class="card">

            <div class="card-header header-elements-inline">
                <h5 class="card-title">ایجاد بسته پیامکی</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a href="{{ route('admin.package.index') }}"><button class="list-icons-item text-danger"><i
                                    class="icon-arrow-left12 icon-2x"></i></button></a>
                    </div>
                </div>
            </div>


            <div class="card-body">


                <form action="{{ route('admin.package.store') }}" method="POST">

                    @csrf

                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">قیمت</label>
                        <div class="col-lg-9">
                            <input type="text" name="price" class="form-control">
                            @error('price')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">تعداد</label>
                        <div class="col-lg-9">
                            <input type="text" name="count" class="form-control">
                            @error('count')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="text-right">
                        <button type="submit" class="btn btn-success">ایجاد
                            <i class="icon-paperplane ml-2"></i></button>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>
@endsection