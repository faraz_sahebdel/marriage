@extends('layouts.admin')

@section('title','ویرایش بسته پیامکی')

@section('content')

<div class="row">

    <div class="col-md-6">
        <div class="card">

            <div class="card-header header-elements-inline">
                <h5 class="card-title">بسته پیامکی</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <form action="{{ route('admin.package.destroy', $package) }}"
                            onsubmit="return confirm('آیا می خواهید بسته پیامکی مورد نظر حذف شود؟')" method="POST">
                            @csrf
                            @method('delete')
                            <button class="list-icons-item text-danger"><i class="icon-cross icon-2x"></i></button>
                        </form>
                        <a href="{{ route('admin.package.index') }}"><button class="list-icons-item text-danger"><i
                                    class="icon-arrow-left12 icon-2x"></i></button></a>
                    </div>
                </div>
            </div>


            <div class="card-body">


                <form action="{{ route('admin.package.update', $package) }}" method="POST">

                    @csrf

                    @method('PUT')

                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">قیمت</label>
                        <div class="col-lg-9">
                            <input type="text" name="price" class="form-control" value="{{ $package->price }}">
                            @error('price')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">تعداد</label>
                        <div class="col-lg-9">
                            <input type="text" name="count" class="form-control" value="{{ $package->count }}">
                            @error('count')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="text-right">
                        <button type="submit" class="btn btn-success">ذخیره
                            <i class="icon-paperplane ml-2"></i></button>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>
@endsection