@extends('layouts.admin')

@section('title','بسته پیامکی')

@section('content')

<!-- Task manager table -->
<div class="card p-1">
    <div class="card-header bg-transparent header-elements-inline mb-1">
        <h6 class="card-title">بسته پیامکی <a href="{{ route('admin.package.create') }}"><i class="icon-plus2 icon-2x text-success"></i></a></h6>
    </div>

    <table class="table tasks-list table-lg" id="laravel_datatable">
        <thead>
            <tr>
                <th>شماره</th>
                <th>قیمت</th>
                <th>تعداد</th>
                <th class="text-muted"><i class="icon-gear"></i></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($packages as $package)
            <tr>
                <td>{{ $package->id }}</td>
                <td>{{ $package->price }}</td>
                <td>{{ $package->count }}</td>
                <td><a href="{{ route('admin.package.edit', $package) }}">مشاهده</a></td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
<!-- /task manager table -->

@endsection