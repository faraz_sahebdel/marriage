@extends('layouts.admin')

@section('title','گزارشات')

@section('content')

<!-- Task manager table -->
<div class="card p-1">
    <div class="card-header bg-transparent header-elements-inline mb-1">
    <h6 class="card-title">گزارشات</h6>
    </div>

    <table class="table tasks-list table-lg" id="laravel_datatable">
        <thead>
            <tr>
                <th>شماره گزارش دهنده</th>
                <th>گزارش دهنده</th>
                <th>شماره گزارش شده</th>
                <th>گزارش شده</th>
                <th>نوع گزارش</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>
<!-- /task manager table -->

@endsection

@push('script')

<script src="/js/data_table/jquery.dataTables.min.js"></script>

<script>
    $(document).ready( function () {
            $('#laravel_datatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('admin.report.list') }}",
                language: { "url": "/json/persian.json" },
                order: [1,'asc'],
                pageLength: 15,
                columns: [
                            { data: 'user_reporter_id', name: 'user_reporter_id' },
                            { data: 'user_reporter.nick_name', name: 'user_reporter.nick_name' },
                            { data: 'user_reported_id', name: 'user_reported_id' },
                            { data: 'user_report.nick_name', name: 'user_report.nick_name' },
                            { data: 'report', name: 'report' },
                        ]
                });
            });
</script>
@endpush