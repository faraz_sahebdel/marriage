@extends('layouts.admin')

@section('title','')

@section('content')

<div class="row">

    <div class="col-md-6">
        <div class="card">



            <div class="card-body">


                <form action="{{ route('admin.request.offuce.save', $request) }}" method="POST">

                    @csrf
                   
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">نام دفتر</label>
                        <div class="col-lg-9">
                            <select name="name" class="form-control">
                                @foreach ($offices as $office)

                                <option value="{{ $office->id }}">
                                    {{ $office->name }}
                                </option>
                                @endforeach
                            </select>
                            @error('name')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                  

                    <div class="text-right">
                        <button type="submit" class="btn btn-success">ذخیره
                            <i class="icon-paperplane ml-2"></i></button>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>
@endsection