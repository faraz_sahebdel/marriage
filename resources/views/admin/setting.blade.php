@extends('layouts.admin')

@section('title','داشبورد')

@section('content')

<!-- Main charts -->
<div class="row">


    <div class="card">

        <div class="card-header header-elements-inline">
            <legend>
                <h5 class="card-title">تغییر رمز عبور</h5>
            </legend>
        </div>
        <div class="card-body">

            <form action="{{ route('admin.password.reset') }}" method="POST">

                @csrf

                <p class="mb-10">
                        بعد از تغییر رمز عبور باید وارد شوید. لطفا با دقت رمز عبور را وارد کنید. 
                </p>

                <fieldset class="mb-3">

                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">رمز عبور قبلی</label>
                        <div class="col-lg-9">
                            <input type="password" name="old_password" class="form-control">
                            @error('old_password')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">رمز عبور جدید</label>
                        <div class="col-lg-9">
                            <input type="password" name="password" class="form-control">
                            @error('password')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">تکرار رمز عبور جدید</label>
                        <div class="col-lg-9">
                            <input type="password" name="password_confirmation" class="form-control">
                            @error('password_confirmation')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>


                </fieldset>
                <div class="text-right">
                    <button type="submit" class="btn btn-success">ذخیره <i class="icon-paperplane ml-2"></i></button>
                </div>

            </form>
        </div>
    </div>


</div>

<!-- /main charts -->

@endsection