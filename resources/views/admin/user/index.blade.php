@extends('layouts.admin')

@section('title','کاربران عادی')

@section('content')

<!-- Task manager table -->
<div class="card p-1">
    <div class="card-header bg-transparent header-elements-inline mb-1">
        <h6 class="card-title">کاربران <a href="{{ route('admin.user.create') }}"><i class="icon-plus2 icon-2x text-success"></i></a></h6>
    </div>

    <table class="table tasks-list table-lg" id="laravel_datatable">
        <thead>
            <tr>
                <th>شماره</th>
                <th>اسم مستعار</th>
                <th>محل زندگی</th>
                <th class="text-muted"><i class="icon-gear"></i></th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>
<!-- /task manager table -->

@endsection

@push('script')

<script src="/js/data_table/jquery.dataTables.min.js"></script>

<script>
    $(document).ready( function () {
            $('#laravel_datatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('admin.user.list') }}",
                language: { "url": "/json/persian.json" },
                order: [1,'asc'],
                pageLength: 15,
                columns: [
                            { data: 'id', name: 'id' },
                            { data: 'nick_name', name: 'nick_name' },
                            { data: 'city.name', name: 'city.name' },
                            { data: 'action', name: 'action', orderable: false, searchable: false},
                        ]
                });
            });
</script>
@endpush