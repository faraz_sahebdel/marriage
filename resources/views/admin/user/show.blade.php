@extends('layouts.admin')

@section('title','')

@section('content')

<div class="row">

    <div class="col-md-6">
        <div class="card">

            <div class="card-header header-elements-inline">
                <h5 class="card-title">کاربر: {{ $user->nick_name }}</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        @if (!$user->block_admin)
                        <form action="{{ route('admin.admin.block.add', $user) }}"
                            onsubmit="return confirm('آیا می خواهید کاربر مورد نظر مسدود شود؟')" method="POST">
                            @csrf
                            <button class="list-icons-item text-danger"><i class="icon-blocked icon-2x"></i></button>
                        </form>
                        @endif

                        <form action="{{ route('admin.user.destroy', $user) }}"
                            onsubmit="return confirm('آیا می خواهید کاربر مورد نظر حذف شود؟')" method="POST">
                            @csrf
                            @method('delete')
                            <button class="list-icons-item text-danger"><i class="icon-cross icon-2x"></i></button>
                        </form>
                        <a href="{{ route('admin.user.index') }}"><button class="list-icons-item text-danger"><i
                                    class="icon-arrow-left12 icon-2x"></i></button></a>
                    </div>
                </div>
            </div>


            <div class="card-body">


                <form action="{{ route('admin.user.update', $user) }}" method="POST" enctype="multipart/form-data">

                    @csrf

                    @method('PUT')

                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">اسم مستعار</label>
                        <div class="col-lg-9">
                            <input type="text" name="nick_name" class="form-control" value="{{ $user->nick_name }}">
                            @error('nick_name')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">تلفن همراه</label>
                        <div class="col-lg-9">
                            <input type="text" name="mobile" class="form-control" value="{{ $user->mobile }}">
                            <span class="text-warning">اخطار: در صورت تغییر شماره تلفن همراه باید فعال سازی
                                کنید.</span><br>
                            @error('mobile')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    {{-- <div class="form-group row">
                        <label class="col-form-label col-lg-3">آپلود عکس</label>
                        <div class="col-lg-9">
                            <a href="{{ '/uploads/' . $user->image }}" target="_blank"><img
                                    src="{{ '/uploads/' . $user->image }}"
                                    class="img-fluid rounded-circle shadow-1 mb-3" width="100" height="100" alt=""></a>
                            <input type="file" name="image" class="form-control-uniform-custom">
                            @error('image')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div> --}}

                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">جنسیت</label>

                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="gender" value="0"
                                    {{ ($user->gender == 0 ? 'checked' : '') }}>
                                خانم
                            </label>
                        </div>

                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="gender" value="1"
                                    {{ ($user->gender == 1 ? 'checked' : '') }}>
                                آقا
                            </label>
                        </div>
                    </div>


                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">شهر زندگی</label>
                        <div class="col-lg-9">
                            <select name="city_id" class="form-control">
                                @foreach ($cities as $city)
                                <optgroup label="{{ $city->name }}">
                                    @foreach ($city->cities as $c)
                                    <option value="{{ $c->id }}" {{ ($user->city_id == $c->id ? 'selected' : '')}}>
                                        {{ $c->name }}
                                    </option>
                                    @endforeach
                                </optgroup>
                                @endforeach
                            </select>
                            @error('city_id')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">سن</label>
                        <div class="col-lg-9">
                            <input type="text" name="age" class="form-control" data-mask="1999/99/99"
                                value="{{ $user->age }}" style="direction: ltr">
                            @error('age')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">وضعیت ازدواج</label>
                        <div class="col-lg-9">
                            <select name="marital_status" class="form-control">
                                @foreach (App\Model\User::MARITAL_STATUS_LIST as $key => $marital_status)

                                <option value="{{ $key }}" {{ $user->marital_status == $key ? 'selected' : '' }}>
                                    {{ $marital_status }}
                                </option>
                                @endforeach
                            </select>
                            @error('marital_status')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">قد</label>
                        <div class="col-lg-9">
                            <input type="text" name="height" class="form-control" value="{{ $user->height }}">
                            <span class="text-info">واحد شمارش سانتی متر است.</span><br>
                            @error('height')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">وزن</label>
                        <div class="col-lg-9">
                            <input type="text" name="weight" class="form-control" value="{{ $user->weight }}">
                            @error('weight')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">تحصیلات</label>
                        <div class="col-lg-9">
                            <select name="education" class="form-control">
                                @foreach (App\Model\User::EDUCATION_LIST as $key => $education)

                                <option value="{{ $key }}" {{ $user->education == $key ? 'selected' : '' }}>
                                    {{ $education }}
                                </option>
                                @endforeach
                            </select>
                            @error('education')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">محل اقامت</label>
                        <div class="col-lg-9">
                            <select name="residence" class="form-control">
                                @foreach (App\Model\User::RESIDENCE_STATUS_LIST as $key => $residence)

                                <option value="{{ $key }}" {{ $user->residence == $key ? 'selected' : '' }}>
                                    {{ $residence }}
                                </option>
                                @endforeach
                            </select>
                            @error('residence')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">وضعیت اشتغال</label>
                        <div class="col-lg-9">
                            <select name="employment_status" class="form-control">
                                @foreach (App\Model\User::EMPLOYMENT_STATUS_LIST as $key => $employment)

                                <option value="{{ $key }}" {{ $user->employment_status == $key ? 'selected' : '' }}>
                                    {{ $employment }}
                                </option>
                                @endforeach
                            </select>
                            @error('employment_status')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">عقاید مذهبی</label>
                        <div class="col-lg-9">
                            <select name="religious" class="form-control">
                                @foreach (App\Model\User::RELIGIOUS_LIST as $key => $religious)

                                <option value="{{ $key }}" {{ $user->religious == $key ? 'selected' : '' }}>
                                    {{ $religious }}
                                </option>
                                @endforeach
                            </select>
                            @error('religious')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">نوع مهریه</label>
                        <div class="col-lg-9">
                            <select name="dowry" class="form-control">
                                @foreach (App\Model\User::DOWRY_LIST as $key => $dowry)

                                <option value="{{ $key }}" {{ $user->dowry == $key ? 'selected' : '' }}>
                                    {{ $dowry }}
                                </option>
                                @endforeach
                            </select>
                            @error('dowry')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>


                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">میزان مهریه</label>
                        <div class="col-lg-9">
                            <input type="text" name="dowry_cash" class="form-control">
                            <span class="text-info">واحد تومان است.</span><br>
                            @error('dowry_cash')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">تعداد فرزند</label>
                        <div class="col-lg-9">
                            <input type="text" name="child" class="form-control">
                            @error('child')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">توضیح خلاصه از خود</label>
                        <div class="col-lg-9">
                            <textarea type="text" name="story" class="form-control" rows="6"></textarea>
                            @error('story')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="text-right">
                        <button type="submit" class="btn btn-success">ذخیره
                            <i class="icon-paperplane ml-2"></i></button>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>
@endsection