<!DOCTYPE html>
<html lang="en" dir="rtl">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>@yield('title')</title>

	<!-- Global stylesheets -->
	<link href="/css/roboto.css" rel="stylesheet" type="text/css">
	<link href="/css/icomoon.min.css" rel="stylesheet" type="text/css">
	<link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
	<link href="/css/layout.min.css" rel="stylesheet" type="text/css">
	<link href="/css/components.min.css" rel="stylesheet" type="text/css">
	<link href="/css/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script src="/js/main/jquery.min.js"></script>
	<script src="/js/main/bootstrap.bundle.min.js"></script>
	<script src="/js/plugins/loaders/blockui.min.js"></script>
	<script src="/js/plugins/ui/ripple.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script src="/js/plugins/visualization/d3/d3.min.js"></script>
	<script src="/js/plugins/visualization/d3/d3_tooltip.js"></script>
	<script src="/js/plugins/forms/styling/switchery.min.js"></script>
	<script src="/js/plugins/ui/moment/moment.min.js"></script>
	<script src="/js/plugins/pickers/daterangepicker.js"></script>

	<script src="/js/app.js"></script>
	<script src="/js/demo_pages/dashboard.js"></script>
	<script src="/js/demo_charts/pages/dashboard/light/streamgraph.js"></script>
	<script src="/js/demo_charts/pages/dashboard/light/sparklines.js"></script>
	<script src="/js/demo_charts/pages/dashboard/light/lines.js"></script>
	<script src="/js/demo_charts/pages/dashboard/light/areas.js"></script>
	<script src="/js/demo_charts/pages/dashboard/light/donuts.js"></script>
	<script src="/js/demo_charts/pages/dashboard/light/bars.js"></script>
	<script src="/js/demo_charts/pages/dashboard/light/progress.js"></script>
	<script src="/js/demo_charts/pages/dashboard/light/heatmaps.js"></script>
	<script src="/js/demo_charts/pages/dashboard/light/pies.js"></script>
	<script src="/js/demo_charts/pages/dashboard/light/bullets.js"></script>
	<script src="/js/plugins/forms/inputs/inputmask.js"></script>

	<!-- /theme JS files -->

	@stack('script')

</head>

<body>

	@include('layouts.admin_navbar')

	<!-- Page content -->
	<div class="page-content">

		@include('layouts.admin_sidebar')

		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Content area -->
			<div class="content">

				@include('layouts.flash-message')
				
				@yield('content')

			</div>
			<!-- /content area -->


			<!-- Footer -->
			<div class="navbar navbar-expand-lg navbar-light">
				<div class="text-center d-lg-none w-100">
					<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse"
						data-target="#navbar-footer">
						<i class="icon-unfold mr-2"></i>
						پاورقی
					</button>
				</div>

				<div class="navbar-collapse collapse" id="navbar-footer">
					<span class="navbar-text">
						&copy; 2015 - 2018. <a href="#">Limitless Web App Kit</a> by <a
							href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
					</span>

					<ul class="navbar-nav ml-lg-auto">
						<li class="nav-item"><a href="https://kopyov.ticksy.com/" class="navbar-nav-link"
								target="_blank"><i class="icon-lifebuoy mr-2"></i> Support</a></li>
						<li class="nav-item"><a href="http://demo.interface.club/limitless/docs/"
								class="navbar-nav-link" target="_blank"><i class="icon-file-text2 mr-2"></i> Docs</a>
						</li>
						<li class="nav-item"><a
								href="https://themeforest.net/item/limitless-responsive-web-application-kit/13080328?ref=kopyov"
								class="navbar-nav-link font-weight-semibold"><span class="text-pink-400"><i
										class="icon-cart2 mr-2"></i> Purchase</span></a></li>
					</ul>
				</div>
			</div>
			<!-- /footer -->

		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->

</body>

</html>