<!-- Main sidebar -->
<div class="sidebar sidebar-light sidebar-main sidebar-expand-md">

    <!-- Sidebar mobile toggler -->
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-arrow-right8"></i>
        </a>
        <span class="font-weight-semibold">منوی کاربری</span>
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>
    <!-- /sidebar mobile toggler -->


    <!-- Sidebar content -->
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user-material">
            <div class="sidebar-user-material-body">
                <div class="card-body text-center">
                    <a href="#">
                        {{-- <img src="/images/placeholders/placeholder.jpg" class="img-fluid rounded-circle shadow-1 mb-3" width="80" height="80" alt=""> --}}
                    </a>
                    <h6 class="mb-0 text-white text-shadow-dark">مدیر</h6>
                    <span class="font-size-sm text-white text-shadow-dark">{{ auth('admin')->user()->email }}</span>
                </div>

                <div class="sidebar-user-material-footer">
                    <a href="#user-nav"
                        class="d-flex justify-content-between align-items-center text-shadow-dark dropdown-toggle"
                        data-toggle="collapse"><span>حساب کاربری</span></a>
                </div>
            </div>

            <div class="collapse" id="user-nav">
                <ul class="nav nav-sidebar">
                    {{-- <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="icon-user-plus"></i>
                            <span>پروفایل من</span>
                        </a>
                    </li> --}}
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="icon-coins"></i>
                            <span>کیف پول من</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="icon-comment-discussion"></i>
                            <span>پیامها</span>
                            <span class="badge bg-teal-400 badge-pill align-self-center ml-auto">58</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.setting') }}" class="nav-link">
                            <i class="icon-cog5"></i>
                            <span>تنظیمات کاربری</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.logout') }}" class="nav-link">
                            <i class="icon-switch2"></i>
                            <span>خروج</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /user menu -->


        <!-- Main navigation -->
        <div class="card card-sidebar-mobile">
            <ul class="nav nav-sidebar" data-nav-type="accordion">

                <!-- Main -->
                <li class="nav-item">
                    <a href="{{ route('admin.index') }}" class="nav-link active">
                        <i class="icon-home4"></i>
                        <span>
                            میزکار
                        </span>
                    </a>
                </li>
                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><i class="icon-copy"></i> <span>کاربران</span></a>

                    <ul class="nav nav-group-sub" data-submenu-title="Layouts">
                        <li class="nav-item"><a href="{{ route('admin.user.index') }}" class="nav-link active">کاربران
                                عادی</a></li>
                        <li class="nav-item"><a href="{{ route('admin.office.index') }}" class="nav-link">دفاتر
                                ازدواج</a></li>
                        <li class="nav-item"><a href="{{ route('admin.admins.index') }}" class="nav-link">مدیران</a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><i class="icon-stack"></i> <span>پلیس سامانه</span></a>

                    <ul class="nav nav-group-sub" data-submenu-title="Starter kit">
                        <li class="nav-item"><a href="{{ route('admin.report.index') }}" class="nav-link">گزارشات</a>
                        </li>
                        <li class="nav-item"><a href="{{ route('admin.admin.block') }}" class="nav-link">کاربران بلاک
                                شده توسط مدیر</a></li>
                        <li class="nav-item"><a href="{{ route('admin.block.index') }}" class="nav-link">کاربران مسدود
                                شده توسط کاربران</a></li>
                    </ul>
                </li>
                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><i class="icon-color-sampler"></i> <span>محتوای سایت</span></a>

                    <ul class="nav nav-group-sub" data-submenu-title="Themes">
                        <li class="nav-item"><a href="index.html" class="nav-link active">برگه ها</a></li>
                        <li class="nav-item"><a href="../../../RTL/default/full/index.html" class="nav-link">دسته بندی
                                مطالب</a></li>
                        <li class="nav-item"><a href="index.html" class="nav-link active">نوشته ها</a></li>
                        <li class="nav-item"><a href="index.html" class="nav-link active">اسلایدر ها</a></li>
                        <li class="nav-item"><a href="index.html" class="nav-link active">منوها</a></li>
                    </ul>
                </li>
                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><i class="icon-color-sampler"></i> <span>برگه ها</span></a>

                    <ul class="nav nav-group-sub" data-submenu-title="Themes">
                        <li class="nav-item"><a href="../../../RTL/default/full/index.html" class="nav-link">دسته بندی
                                مطالب</a></li>
                        <li class="nav-item"><a href="index.html" class="nav-link active">لیست مطالب</a></li>
                        <li class="nav-item"><a href="index.html" class="nav-link active">اسلایدر ها</a></li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.alert.index') }}" class="nav-link"><i class="icon-stack"></i> <span>اعلان
                            ها</span></a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('admin.request.index') }}" class="nav-link"><i class="icon-stack"></i> <span>درخواست ها</span></a>
                </li>
                
                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><i class="icon-stack"></i> <span>مالی</span></a>

                    <ul class="nav nav-group-sub" data-submenu-title="Starter kit">
                        <li class="nav-item"><a href="{{ route('admin.financial.success') }}" class="nav-link">پرداخت
                                های موفق</a></li>
                        <li class="nav-item"><a href="{{ route('admin.financial.fail') }}" class="nav-link">پرداخت های
                                ناموفق </a></li>
                    </ul>
                </li>
                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><i class="icon-stack"></i> <span>تنظیمات</span></a>

                    <ul class="nav nav-group-sub" data-submenu-title="Starter kit">
                        <li class="nav-item"><a href="{{ route('admin.package.index') }}" class="nav-link">بسته
                                پیامکی</a></li>
                        <li class="nav-item"><a href="../seed/layout_nav_horizontal.html" class="nav-link">کاربران</a>
                        </li>
                        <li class="nav-item"><a href="../seed/sidebar_none.html" class="nav-link">پلیس سامانه</a></li>
                        <li class="nav-item"><a href="{{ route('admin.config') }}" class="nav-link">تنظیمات سایت</a>
                        </li>
                        <li class="nav-item"><a href="../seed/sidebar_main.html" class="nav-link">اعلان ها</a></li>
                        <li class="nav-item"><a href="../seed/sidebar_main.html" class="nav-link">مالی</a></li>
                    </ul>
                </li>
                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><i class="icon-stack"></i> <span>گزارشات</span></a>

                    <ul class="nav nav-group-sub" data-submenu-title="Starter kit">
                        <li class="nav-item"><a href="../seed/layout_nav_horizontal.html" class="nav-link">آمار کلی
                                سیستم</a></li>
                        <li class="nav-item"><a href="{{ route('admin.financial.all') }}" class="nav-link">گزارشات
                                مالی</a></li>
                        <li class="nav-item"><a href="../seed/sidebar_none.html" class="nav-link">نمودار ها</a></li>
                        <li class="nav-item"><a href="../seed/sidebar_none.html" class="nav-link">فعالیت ها</a></li>
                    </ul>
                </li>
                <!-- /main -->

                <li class="nav-item">
                    <a href="{{ route('admin.ticket.index.list') }}" class="nav-link">
                        <i class="text-danger icon-heart6"></i>
                        <span>تیکت ها</span></a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('admin.log.index') }}" class="nav-link">
                        <i class="text-danger icon-heart6"></i>
                        <span>لاگ ها</span></a>
                </li>

            </ul>
        </div>
        <!-- /main navigation -->

    </div>
    <!-- /sidebar content -->

</div>
<!-- /main sidebar -->