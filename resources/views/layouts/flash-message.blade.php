@if ($message = Session('success'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{!! $message !!}</strong>
</div>
@endif


@if ($message = Session('error'))
<div class="alert alert-danger alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{!! $message !!}</strong>
</div>
@endif


@if ($message = Session('warning'))
<div class="alert alert-info alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
	<strong>{!! $message !!}</strong>
</div>
@endif


@if ($message = Session('info'))
<div class="alert alert-info alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
	<strong>{!! $message !!}</strong>
</div>
@endif


{{-- @if ($errors->any())
<div class="alert alert-danger">
	<button type="button" class="close" data-dismiss="alert">×</button>	
	شما خطا دارید!
</div>
@endif --}}