<!-- Main sidebar -->
<div class="sidebar sidebar-light sidebar-main sidebar-expand-md">

    <!-- Sidebar mobile toggler -->
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-arrow-right8"></i>
        </a>
        <span class="font-weight-semibold">منوی کاربری</span>
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>
    <!-- /sidebar mobile toggler -->


    <!-- Sidebar content -->
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user-material">
            <div class="sidebar-user-material-body">
                <div class="card-body text-center">
                    @isset(auth()->guard('user')->user()->image)
                    <a href="#">
                        <img src="{{ '/uploads/' . auth()->guard('user')->user()->image }}"
                            class="img-fluid rounded-circle shadow-1 mb-3" width="100" height="100" alt="">
                    </a>
                    @endisset
                    <h6 class="mb-0 text-white text-shadow-dark">کاربر</h6>
                    <span
                        class="font-size-sm text-white text-shadow-dark">{{ auth()->guard('user')->user()->nick_name }}</span>
                </div>

                <div class="sidebar-user-material-footer">
                    <a href="#user-nav"
                        class="d-flex justify-content-between align-items-center text-shadow-dark dropdown-toggle"
                        data-toggle="collapse"><span>حساب کاربری</span></a>
                </div>
            </div>

            <div class="collapse" id="user-nav">
                <ul class="nav nav-sidebar">
                    <li class="nav-item">
                        <a href="{{ route('user.info.update') }}" class="nav-link">
                            <i class="icon-user-plus"></i>
                            <span>پروفایل من</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="icon-coins"></i>
                            <span>کیف پول من</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="icon-comment-discussion"></i>
                            <span>پیامها</span>
                            <span class="badge bg-teal-400 badge-pill align-self-center ml-auto">58</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('user.setting') }}" class="nav-link">
                            <i class="icon-cog5"></i>
                            <span>تنظیمات کاربری</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('user.logout') }}" class="nav-link">
                            <i class="icon-switch2"></i>
                            <span>خروج</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /user menu -->


        <!-- Main navigation -->
        <div class="card card-sidebar-mobile">
            <ul class="nav nav-sidebar" data-nav-type="accordion">

                <!-- Main -->
                <li class="nav-item">
                    <a href="{{ route('user.index') }}" class="nav-link active">
                        <i class="icon-home4"></i>
                        <span>
                            میزکار
                        </span>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('user.spouse') }}" class="nav-link"><i class="text-danger icon-heart6"></i>
                        <span>یافتن همسر</span></a>
                </li>

                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><i class="icon-stack"></i> <span>درخواست ها</span></a>

                    <ul class="nav nav-group-sub" data-submenu-title="Starter kit">
                        <li class="nav-item"><a href="{{ route('user.request.get') }}" class="nav-link">درخواست های
                                دریافت شده</a></li>
                        <li class="nav-item"><a href="{{ route('user.request.send') }}" class="nav-link">درخواست های
                                ارسال شده</a></li>
                    </ul>
                </li>

                <li class="nav-item">
                    <a href="{{ route('user.favorite.index') }}" class="nav-link">
                        <i class="text-danger icon-heart6"></i>
                        <span>علاقه مندی ها</span></a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('user.block.index') }}" class="nav-link">
                        <i class="text-danger icon-heart6"></i>
                        <span>مسدودی ها</span></a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('user.package') }}" class="nav-link">
                        <i class="text-danger icon-heart6"></i>
                        <span>خرید بسته پیامکی</span></a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('user.order.index') }}" class="nav-link">
                        <i class="text-danger icon-heart6"></i>
                        <span>پرداخت ها</span></a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('user.ticket.index.list') }}" class="nav-link">
                        <i class="text-danger icon-heart6"></i>
                        <span>تیکت ها</span></a>
                </li>

                <!-- /main -->
            </ul>
        </div>
        <!-- /main navigation -->
    </div>
    <!-- /sidebar content -->

</div>
<!-- /main sidebar -->