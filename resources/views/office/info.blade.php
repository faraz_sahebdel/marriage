@extends('layouts.office')

@section('title','داشبورد')

@section('content')


<!-- Main charts -->
<div class="row">

    <div class="col-md-6">

        <div class="card">

            <div class="card-header header-elements-inline">
                <legend>
                    <h5 class="card-title">اطلاعات شخصی</h5>
                </legend>
            </div>
            <div class="card-body">

                <form method="POST" action="{{ route('office.info.update') }}">
                    @csrf

                    <div class="form-group row">
                        <label for="first_name"
                            class="col-md-4 col-form-label text-md-right">{{ __('auth.first_name') }}</label>

                        <div class="col-md-6">
                            <input id="first_name" type="text"
                                class="form-control @error('first_name') is-invalid @enderror" name="first_name"
                                value="{{ $office->first_name }}" required autocomplete="first_name" autofocus>

                            @error('first_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="last_name"
                            class="col-md-4 col-form-label text-md-right">{{ __('auth.last_name') }}</label>

                        <div class="col-md-6">
                            <input id="last_name" type="text"
                                class="form-control @error('last_name') is-invalid @enderror" name="last_name"
                                value="{{ $office->last_name }}" required autocomplete="last_name" autofocus>

                            @error('last_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="phone" class="col-md-4 col-form-label text-md-right">تلفن</label>

                        <div class="col-md-6">
                            <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror"
                                name="phone" value="{{ $office->phone }}" required autocomplete="phone" autofocus>

                            @error('phone')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>


                    <div class="form-group row">
                        <label class="col-md-4 col-form-label text-md-right">شهر زندگی</label>
                        <div class="col-lg-6">
                            <select name="city_id" class="form-control">
                                @foreach ($cities as $city)
                                <optgroup label="{{ $city->name }}">
                                    @foreach ($city->cities as $c)
                                    <option value="{{ $c->id }}" {{ (auth('office')->user()->city_id == $c->id ? 'selected' : '')}}>
                                        {{ $c->name }}
                                    </option>
                                    @endforeach
                                </optgroup>
                                @endforeach
                            </select>
                            @error('city_id')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="address" class="col-md-4 col-form-label text-md-right">آدرس</label>

                        <div class="col-md-6">
                            <textarea id="address" type="text"
                                class="form-control @error('address') is-invalid @enderror" name="address" required
                                autocomplete="address">{{ $office->address }}</textarea>

                            @error('address')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="mobile"
                            class="col-md-4 col-form-label text-md-right">{{ __('auth.mobile') }}</label>

                        <div class="col-md-6">
                            <input id="mobile" type="text" class="form-control @error('mobile') is-invalid @enderror"
                                name="mobile" value="{{ $office->mobile }}" required autocomplete="mobile">

                            @error('mobile')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>


                    {{-- <div class="form-group row">
                        <label for="password"
                            class="col-md-4 col-form-label text-md-right">{{ __('auth.password') }}</label>

                        <div class="col-md-6">
                            <input id="password" type="password"
                                class="form-control @error('password') is-invalid @enderror" name="password" required
                                autocomplete="new-password">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div> --}}

                    {{-- <div class="form-group row">
                        <label for="password-confirm"
                            class="col-md-4 col-form-label text-md-right">{{ __('auth.repeat_password') }}</label>

                        <div class="col-md-6">
                            <input id="password-confirm" type="password" class="form-control"
                                name="password_confirmation" required autocomplete="new-password">
                        </div>
                    </div> --}}

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('auth.register') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>

</div>

<!-- /main charts -->

@endsection