@extends('layouts.user')

@section('title','داشبورد')

@section('content')

<!-- Main charts -->
<div class="row">

    @isset($alerts)
    <div class="col-lg-12">
        <div class="alert alert-info alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            @foreach ($alerts as $alert)
            <p>{{ $alert->content }}</p>
            @endforeach
        </div>
    </div>
    @endisset

    <div class="col-sm-6 col-xl-3">
        <div class="card card-body">
            <div class="media">
                <div class="mr-3 align-self-center">
                    <i class="icon-envelop icon-3x text-success-400"></i>
                </div>

                <div class="media-body text-right">
                    <h3 class="font-weight-semibold mb-0">{{ auth('user')->user()->sms_count }}</h3>
                    <span class="text-uppercase font-size-sm text-muted">تعداد موجودی پیامکی</span>
                </div>
            </div>
        </div>
    </div>


    @if(!auth('user')->user()->enable_code)
    <div class="col-sm-6 col-xl-3">
        <div class="card card-body">
            <form action="{{ route('user.enable.code') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label>کد فعال سازی ارسال شده به تلفن همراه خود را وارد فرمائید: </label>
                    <input type="text" name="enable_code" class="form-control" placeholder="کد فعال سازی را وارد کنید.">
                </div>
                <button type="submit" class="btn btn-primary elastic-manual-trigger">فعال سازی</button>
            </form>
        </div>
    </div>
    @endif



</div>

<!-- /main charts -->

@endsection