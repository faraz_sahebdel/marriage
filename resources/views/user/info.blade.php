@extends('layouts.user')

@section('title','داشبورد')

@section('content')


<!-- Main charts -->
<div class="row">


    <div class="card">

        <div class="card-header header-elements-inline">
            <legend>
                <h5 class="card-title">اطلاعات شخصی</h5>
            </legend>
        </div>
        <div class="card-body">

            <form action="{{ route('user.info.save') }}" method="POST" enctype="multipart/form-data">

                @csrf

                <p class="mb-10">
                    فرم اطلاعات خود را کامل کنید. در غیر اینصورت نمی توانید از امکانات سایت استفاده کنید.
                </p>

                <fieldset class="mb-3">

                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">اسم مستعار</label>
                        <div class="col-lg-9">
                            <input type="text" name="nick_name" class="form-control" value="{{ $user->nick_name }}">
                            @error('nick_name')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">تلفن همراه</label>
                        <div class="col-lg-9">
                            <input type="text" name="mobile" class="form-control" value="{{ $user->mobile }}">
                            <span class="text-warning">اخطار: در صورت تغییر شماره تلفن همراه باید فعال سازی
                                کنید.</span><br>
                            @error('mobile')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    {{-- <div class="form-group row">
                        <label class="col-form-label col-lg-3">آپلود عکس</label>
                        <div class="col-lg-9">
                            <input type="file" name="image" class="form-control-uniform-custom">
                            @error('image')
                            <span class="text-danger">{{ $message }}</span>
                    @enderror
        </div>
    </div> --}}

    <div class="form-group row">
        <label class="col-form-label col-lg-3">جنسیت</label>
        <div class="col-lg-9">
            <input type="text" name="gender" class="form-control" value="{{ ($user->gender == 0 ? 'خانم' : 'آقا') }}"
                disabled>
        </div>
    </div>

    <div class="form-group row">
        <label class="col-form-label col-lg-3">شهر زندگی</label>
        <div class="col-lg-9">
            <select name="city_id" class="form-control">
                @foreach ($cities as $city)
                <optgroup label="{{ $city->name }}">
                    @foreach ($city->cities as $c)
                    <option value="{{ $c->id }}" {{ ($user->city_id == $c->id ? 'selected' : '')}}>
                        {{ $c->name }}
                    </option>
                    @endforeach
                </optgroup>
                @endforeach
            </select>
            @error('city_id')
            <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label class="col-form-label col-lg-3">سن</label>
        <div class="col-lg-9">
            <input type="text" name="age" class="form-control" data-mask="1999/99/99" value="{{ $user->age }}"
                style="direction: ltr">
            <span class="text-warning">فرمت صحیح: 1371/02/08</span><br>
            @error('age')
            <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label class="col-form-label col-lg-3">وضعیت ازدواج</label>
        <div class="col-lg-9">
            <select name="marital_status" class="form-control">
                @foreach (App\Model\User::MARITAL_STATUS_LIST as $key => $marital_status)

                <option value="{{ $key }}" {{ $user->marital_status == $key ? 'selected' : '' }}>
                    {{ $marital_status }}
                </option>
                @endforeach
            </select>
            @error('marital_status')
            <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label class="col-form-label col-lg-3">قد</label>
        <div class="col-lg-9">
            <input type="text" name="height" class="form-control" value="{{ $user->height }}">
            <span class="text-info">واحد شمارش سانتی متر است.</span><br>
            @error('height')
            <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label class="col-form-label col-lg-3">وزن</label>
        <div class="col-lg-9">
            <input type="text" name="weight" class="form-control" value="{{ $user->weight }}">
            @error('weight')
            <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label class="col-form-label col-lg-3">تحصیلات</label>
        <div class="col-lg-9">
            <select name="education" class="form-control">
                @foreach (App\Model\User::EDUCATION_LIST as $key => $education)

                <option value="{{ $key }}" {{ $user->education == $key ? 'selected' : '' }}>
                    {{ $education }}
                </option>
                @endforeach
            </select>
            @error('education')
            <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label class="col-form-label col-lg-3">محل اقامت</label>
        <div class="col-lg-9">
            <select name="residence" class="form-control">
                @foreach (App\Model\User::RESIDENCE_STATUS_LIST as $key => $residence)

                <option value="{{ $key }}" {{ $user->residence == $key ? 'selected' : '' }}>
                    {{ $residence }}
                </option>
                @endforeach
            </select>
            @error('residence')
            <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label class="col-form-label col-lg-3">وضعیت اشتغال</label>
        <div class="col-lg-9">
            <select name="employment_status" class="form-control">
                @foreach (App\Model\User::EMPLOYMENT_STATUS_LIST as $key => $employment)

                <option value="{{ $key }}" {{ $user->employment_status == $key ? 'selected' : '' }}>
                    {{ $employment }}
                </option>
                @endforeach
            </select>
            @error('employment_status')
            <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label class="col-form-label col-lg-3">عقاید مذهبی</label>
        <div class="col-lg-9">
            <select name="religious" class="form-control">
                @foreach (App\Model\User::RELIGIOUS_LIST as $key => $religious)

                <option value="{{ $key }}" {{ $user->religious == $key ? 'selected' : '' }}>
                    {{ $religious }}
                </option>
                @endforeach
            </select>
            @error('religious')
            <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label class="col-form-label col-lg-3">نوع مهریه</label>
        <div class="col-lg-9">
            <select name="dowry" class="form-control">
                @foreach (App\Model\User::DOWRY_LIST as $key => $dowry)

                <option value="{{ $key }}" {{ $user->dowry == $key ? 'selected' : '' }}>
                    {{ $dowry }}
                </option>
                @endforeach
            </select>
            @error('dowry')
            <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label class="col-form-label col-lg-3">میزان مهریه</label>
        <div class="col-lg-9">
            <input type="text" name="dowry_cash" class="form-control" value="{{ $user->dowry_cash }}">
            <span class="text-info">واحد تومان است.</span><br>
            @error('dowry_cash')
            <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label class="col-form-label col-lg-3">تعداد فرزند</label>
        <div class="col-lg-9">
            <input type="text" name="child" class="form-control" value="{{ $user->child }}">
            @error('child')
            <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label class="col-form-label col-lg-3">توضیح خلاصه از خود</label>
        <div class="col-lg-9">
            <textarea type="text" name="story" class="form-control" rows="6">{{ $user->story }}</textarea>
            @error('story')
            <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>
    </div>

    </fieldset>
    <div class="text-right">
        <button type="submit" class="btn btn-success">ذخیره <i class="icon-paperplane ml-2"></i></button>
    </div>

    </form>
</div>
</div>


</div>

<!-- /main charts -->

@endsection