@extends('layouts.user')

@section('title','داشبورد')

@section('content')

<!-- Main charts -->
<div class="row">

    @foreach ($pakages as $package)
    <div class="col-sm-6 col-xl-3">
        <div class="card card-body">
            <div class="media text-center">
                

                <form action="{{ route('user.package.buy', $package) }}" method="POST">
                    @csrf
                    <input type="hidden" name="buy" value="{{ $package->id }}">
                    <div class="media-body">
                        <h3 class="font-weight-semibold mb-0 ">تعداد {{ $package->count }}</h3>
                        <span class="text-uppercase font-size-lg">قیمت {{ $package->price }} تومان</span>
                        <br>
                        <button class="btn btn-success" type="submit">خرید</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
    @endforeach


</div>

<!-- /main charts -->

@endsection