@extends('layouts.user')

@section('title','علاقه مندی ها')

@section('content')

<!-- Task manager table -->
<div class="card p-1">
    <div class="card-header bg-transparent header-elements-inline mb-1">
    <h6 class="card-title">علاقه مندی ها</h6>
    </div>

    <table class="table tasks-list table-lg" id="laravel_datatable">
        <thead>
            <tr>
                <th>شماره</th>
                <th>نام</th>
                {{-- <th class="text-muted"><i class="icon-gear"></i></th> --}}
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>
<!-- /task manager table -->

@endsection

@push('script')

<script src="/js/data_table/jquery.dataTables.min.js"></script>

<script>
    $(document).ready( function () {
            $('#laravel_datatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('user.favorite.list') }}",
                language: { "url": "/json/persian.json" },
                order: [1,'asc'],
                pageLength: 15,
                columns: [
                            { data: 'id', name: 'id' },
                            { data: 'user.nick_name', name: 'user.nick_name' },
                            // { data: 'userAcceptor.nick_name', name: 'userAcceptor.nick_name' },
                            // { data: 'created_at', name: 'created_at' },
                            // { data: 'action', name: 'action', orderable: false, searchable: false},
                        ]
                });
            });
</script>
@endpush