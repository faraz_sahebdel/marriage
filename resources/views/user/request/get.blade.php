@extends('layouts.user')

@section('title','درخواست های دریافت شده')

@section('content')

<!-- Task manager table -->
<div class="card p-1">
    <div class="card-header bg-transparent header-elements-inline mb-1">
    <h6 class="card-title">درخواست های دریافت شده</h6>
    </div>

    <table class="table tasks-list table-lg" id="laravel_datatable">
        <thead>
            <tr>
                <th>شماره</th>
                <th>نام</th>
                <th>وضعیت</th>
                <th>تاریخ درخواست</th> 
                <th class="text-muted"><i class="icon-gear"></i></th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>
<!-- /task manager table -->

@endsection

@push('script')

<script src="/js/data_table/jquery.dataTables.min.js"></script>

<script>
    $(document).ready( function () {
            $('#laravel_datatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('user.request.get.list') }}",
                language: { "url": "/json/persian.json" },
                order: [1,'asc'],
                pageLength: 15,
                columns: [
                            { data: 'id', name: 'id' },
                            { data: 'user.nick_name', name: 'user.nick_name' },
                            { data: 'status', name: 'status' },
                            { data: 'created_at', name: 'created_at' },
                            { data: 'action', name: 'action', orderable: false, searchable: false},
                        ]
                });
            });
</script>
@endpush