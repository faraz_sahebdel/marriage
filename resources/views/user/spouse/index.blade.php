@extends('layouts.user')

@section('title','یافتن همسر')

@section('content')

@push('style')
    <style>
        .dataTables_filter,.dataTables_length{
            display: none;
        }
    </style>
@endpush

<!-- Task manager table -->
<div class="card p-1">
    <div class="card-header bg-transparent header-elements-inline mb-1">
        <h6 class="card-title">یافتن همسر</h6>
    </div>


    <div class="row p-2">

        <div class="col-md-3">

            <label class="col-form-label">شهر زندگی</label>
            <div class="col-lg-9">
                <select class="form-control searchCity">
                    <option value="all">انتخاب شهر</option>
                    @foreach ($cities as $city)
                    <optgroup label="{{ $city->name }}">
                        @foreach ($city->cities as $c)
                        <option value="{{ $c->id }}">
                            {{ $c->name }}
                        </option>
                        @endforeach
                    </optgroup>
                    @endforeach
                </select>
            </div>

        </div>

        <div class="col-md-3">

            <label class="col-form-label">بازه سنی</label>
            <div class="col-lg-9">
                <select class="form-control searchAge">
                    <option value="all">انتخاب سن</option>
                    @foreach ($age_ranges as $key => $age_range)
                    <option value="{{ $key }}">
                        {{ $age_range }}
                    </option>
                    @endforeach
                </select>
            </div>

        </div>


        <div class="col-md-3">

            <label class="col-form-label">وضعیت ازدواج</label>
            <div class="col-lg-9">
                <select class="form-control searchMaritalStatus">
                    <option value="all">انتخاب وضعیت ازدواج</option>
                    @foreach ($marital_status_lists as $key => $marital_status_list)
                    <option value="{{ $key }}">
                        {{ $marital_status_list }}
                    </option>
                    @endforeach
                </select>
            </div>

        </div>

        <div class="col-md-3">

            <label class="col-form-label">وضعیت قد</label>
            <div class="col-lg-9">
                <select class="form-control searchHeightStatus">
                    <option value="all">انتخاب وضعیت قد</option>
                    @foreach ($height_range_lists as $key => $height_range_list)
                    <option value="{{ $key }}">
                        {{ $height_range_list }}
                    </option>
                    @endforeach
                </select>
            </div>

        </div>


        <div class="col-md-3">

            <label class="col-form-label">وضعیت وزن</label>
            <div class="col-lg-9">
                <select class="form-control searchWeightStatus">
                    <option value="all">انتخاب وضعیت وزن</option>
                    @foreach ($weight_range_lists as $key => $weight_range_list)
                    <option value="{{ $key }}">
                        {{ $weight_range_list }}
                    </option>
                    @endforeach
                </select>
            </div>

        </div>


        <div class="col-md-3">

            <label class="col-form-label">تحصیلات</label>
            <div class="col-lg-9">
                <select class="form-control searchEducation">
                    <option value="all">انتخاب تحصیلات</option>
                    @foreach ($education_lists as $key => $education_list)
                    <option value="{{ $key }}">
                        {{ $education_list }}
                    </option>
                    @endforeach
                </select>
            </div>

        </div>

        <div class="col-md-3">

            <label class="col-form-label">محل اقامت</label>
            <div class="col-lg-9">
                <select class="form-control searchResidence">
                    <option value="all">انتخاب محل اقامت</option>
                    @foreach ($residence_lists as $key => $residence_list)
                    <option value="{{ $key }}">
                        {{ $residence_list }}
                    </option>
                    @endforeach
                </select>
            </div>

        </div>

        <div class="col-md-3">

            <label class="col-form-label">وضعیت اشتغال</label>
            <div class="col-lg-9">
                <select class="form-control searchEmploymentStatus">
                    <option value="all">انتخاب وضعیت اشتغال</option>
                    @foreach ($employment_status_lists as $key => $employment_status_list)
                    <option value="{{ $key }}">
                        {{ $employment_status_list }}
                    </option>
                    @endforeach
                </select>
            </div>

        </div>

        <div class="col-md-3">

            <label class="col-form-label">عقاید مذهبی</label>
            <div class="col-lg-9">
                <select class="form-control searchReligious">
                    <option value="all">انتخاب عقاید مذهبی</option>
                    @foreach ($religious_lists as $key => $religious_list)
                    <option value="{{ $key }}">
                        {{ $religious_list }}
                    </option>
                    @endforeach
                </select>
            </div>

        </div>

        <div class="col-md-3">

            <label class="col-form-label">نوع مهریه</label>
            <div class="col-lg-9">
                <select class="form-control searchDowry">
                    <option value="all">انتخاب نوع مهریه</option>
                    @foreach ($dowry_lists as $key => $dowry_list)
                    <option value="{{ $key }}">
                        {{ $dowry_list }}
                    </option>
                    @endforeach
                </select>
            </div>

        </div>


    </div>
    {{-- <input type="text" name="name" class="form-control searchName" placeholder="Search for Name Only..."> --}}
    <br>

    <table class="table tasks-list table-lg" id="laravel_datatable">
        <thead>
            <tr>
                <th>نام</th>
                <th>سن</th>
                <th>قد</th>
                <th>وزن</th>
                <th>تحصیلات</th>
                <th>محل اقامت</th>
                <th>وضعیت اشتغال</th>
                <th>عقاید مذهبی</th>
                <th>وضعیت ازدواج</th>
                <th>نوع مهریه</th>
                <th><i class="icon-gear"></i></th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
    </style>
    <!-- /task manager table -->
</div>
@endsection

@push('script')

<script src="/js/data_table/jquery.dataTables.min.js"></script>

<script>
    $(function () {
   
   var table = $('#laravel_datatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('user.spouse.list') }}",
                    data: function (d) {
                        d.city_id = $('.searchCity').val(),
                        d.age = $('.searchAge').val(),
                        d.marital_status = $('.searchMaritalStatus').val(),
                        d.height_status = $('.searchHeightStatus').val(),
                        d.weight_status = $('.searchWeightStatus').val(),
                        d.education = $('.searchEducation').val(),

                        d.residence = $('.searchResidence').val(),
                        d.employment_status = $('.searchEmploymentStatus').val(),
                        d.religious = $('.searchReligious').val(),
                        d.dowry = $('.searchDowry').val()
                    }
                },
                language: { "url": "/json/persian.json" },
                order: [1,'asc'],
                pageLength: 15,
                columns: [
                            { data: 'nick_name', name: 'nick_name' },
                            { data: 'age', name: 'age' },
                            { data: 'height', name: 'height' },
                            { data: 'weight', name: 'weight' },
                            { data: 'education', name: 'education' },
                            { data: 'residence', name: 'residence' },
                            { data: 'employment_status', name: 'employment_status' },
                            { data: 'religious', name: 'religious' },
                            { data: 'marital_status', name: 'marital_status' },
                            { data: 'dowry', name: 'dowry' },
                            { data: 'action', name: 'action' },
                ]
            });
                
        $(".searchCity,.searchAge,.searchMaritalStatus,.searchHeightStatus,.searchWeightStatus,.searchEducation,.searchResidence,.searchEmploymentStatus,.searchReligious,.searchDowry").change(function(){
            table.draw();
        });

        // $.fn.dataTable.ext.errMode = 'none';
    });
            
</script>
@endpush