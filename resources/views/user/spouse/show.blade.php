@extends('layouts.user')

@section('title','مشاهده همسر مورد نظر')

@section('content')

<!-- Main charts -->
<div class="row">


    <div class="card col-md-12 pb-3">

        <div class="card-header header-elements-inline">
            <legend>
                <h5 class="card-title text-left">پروفایل {{ $user->nick_name . ' ' . $age }} ساله</h5>
            </legend>

            <legend>
                <h5 class="card-title text-right">آخرین حضور در تاریخ {{ $last_online }}</h5>
            </legend>
        </div>
        <div class="card-body border">

            <div class="row pt-3">
                <div class="col-md-2">
                    <img src="/uploads/{{ $user->image }}" width="100px" height="100px">
                </div>

                <div class="col-md-2">
                    <p>{{ $user->nick_name }}</p>
                    <p>{{ $age }} ساله</p>
                    <p>استان {{ $city_parent->name }}</p>
                    <p>{{ $user->city->name }}</p>
                </div>

                <div class="col-md-2">
                    <p>{{ $user->weight }} کیلوگرم وزن</p>
                    <p>{{ $user->height }} سانتی متر قد</p>
                    <p>{{ App\model\User::MARITAL_STATUS_LIST[$user->marital_status] }}</p>
                    <p>{{ ($user->child ? 'دارای فرزند' : 'بدون فرزند') }}</p>
                </div>

                <div class="col-md-2">
                    <p>{{ App\model\User::EDUCATION_LIST[$user->education] }}</p>
                    <p>{{ App\model\User::EMPLOYMENT_STATUS_LIST[$user->employment_status] }}</p>
                    <p>{{ App\model\User::RELIGIOUS_LIST[$user->religious] }}</p>
                    <p>{{ App\model\User::RESIDENCE_STATUS_LIST[$user->residence] }}</p>
                </div>

                <div class="col-md-4 text-right">
                    <h3>{{ number_format($user->dowry_cash) }}</h3>
                    <h3>مهریه {{ App\model\User::DOWRY_LIST[$user->dowry] }}</h3>

                    <form action="{{ route('user.user.request', $user) }}" method="POST">
                        @csrf
                        <i class="icon-heart6 ml-2 {{ ($like ? 'text-danger' : '') }}" id="like"
                            value="{{ csrf_token() }}" data="{{ $user->id }}"></i>&nbsp;&nbsp;&nbsp;&nbsp;<button
                            type="submit" class="btn btn-outline-info">درخواست ملاقات</button>
                    </form>
                </div>


            </div>
        </div>

        <div class="row mt-3">


            <div class="col-md-12">
                <p class="text-justify">{{ $user->story }}</p>
            </div>

            <div class="col-md-12">
                @isset($model_request)

                @php
                $request_date = new Verta($model_request->created_at);
                $request_date = $request_date->format('j F Y');
                @endphp

                <div class="alert alert-warning" role="alert">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>در خواست شما برای ایشان در تاریخ {{ $request_date }} ارسال شده و ایشان هنوز به درخواست شما
                        پاسخی نداده</strong>
                </div>
                @endisset

            </div>

            <div class="col-md-12 text-right">

                <div class="row text-right">
                    <div class="col-md-10">
                        <form action="{{ route('user.block', $user) }}" method="POST">
                            @csrf
                            <button type="submit" class="btn btn-outline-info">بلاک کردن کاربر</button>
                        </form>
                    </div>
                    <div class="col-md-2">

                        <button type="submit" class="btn btn-outline-info" data-toggle="modal"
                            data-target="#report">گزارش پروفایل به مدیر</button>


                        <!-- Modal -->
                        <div class="modal fade" id="report" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">گزارش کاربر</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>

                                    <form action="{{ route('user.report') }}" method="POST" id="form1" role="form">
                                        @csrf

                                        <input type="hidden" name="id" value="{{ $user->id }}">
                                        <div class="modal-body">
                                            <select class="custom-select" name="report_id">
                                                @foreach ($reports as $key =>$report)
                                                <option value="{{ $key }}">{{ $report }}</option>
                                                @endforeach
                                            </select>
                                        </div>



                                        <div class="modal-footer">
                                            <button type="submit" form="form1" class="btn btn-primary">ارسال</button>

                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">منصرف
                                                شدم</button>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

            <div class="col-md-12 text-left mt-2">
                <span>دفعات بازدید من از این صفحه: {{ $visit->count }}</span>
            </div>
        </div>

    </div>


</div>

<!-- /main charts -->

@endsection

@push('script')

<script>
    $(document).ready( function () {
        $('#like').click(function(){
            var token = $(this).attr('value');
            var user_id = $(this).attr('data');
            if($(this).hasClass('text-danger')){
                $(this).removeClass('text-danger');
                sendDataLike(false, token, user_id);
            }else{
                $(this).addClass('text-danger');
                sendDataLike(true, token, user_id);
            }
        });

    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function sendDataLike(like, token, user_id)
    {
        $.ajax({
            url: "{{ route('user.request.like') }}",
            type: 'POST',
            data: {
                user_id: user_id,
                message: like
            },
            dataType: 'JSON',
            success: function (data) { 
                alert(data.msg);
            }
        }); 
    }
    
</script>

@endpush