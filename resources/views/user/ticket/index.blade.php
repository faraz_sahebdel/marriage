@extends('layouts.user')

@section('title','تیکت ها')

@section('content')

<!-- Task manager table -->
<div class="card p-1">
    <div class="card-header bg-transparent header-elements-inline mb-1">
    <h6 class="card-title">تیکت ها <span class="text-success"><a href="{{ route('user.ticket.send') }}"><i class="icon-plus-circle2"></i></a></span></h6>
    </div>

    <table class="table tasks-list table-lg" id="laravel_datatable">
        <thead>
            <tr>
                <th>شماره</th>
                <th>نام ادمین</th>
                <th>زمان</th>
                {{-- <th>تعداد</th>
                <th>زمان</th>
                <th>وضعیت</th> --}}
                <th class="text-muted"><i class="icon-gear"></i></th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>
<!-- /task manager table -->

@endsection

@push('script')

<script src="/js/data_table/jquery.dataTables.min.js"></script>

<script>
    $(document).ready( function () {
            $('#laravel_datatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('user.ticket.index.lists') }}",
                language: { "url": "/json/persian.json" },
                order: [1,'asc'],
                pageLength: 15,
                columns: [
                            { data: 'id', name: 'id' },
                            { data: 'status', name: 'status' },
                            { data: 'created_at', name: 'created_at' },
                            // { data: 'count', name: 'count' },
                            // { data: 'created_at', name: 'created_at' },
                            { data: 'action', name: 'action' },
                        ]
                });
            });
</script>
@endpush