@extends('layouts.user')

@section('title','تیکت')

@section('content')

@php
use Hekmatinasser\Verta\Facades;

$id = isset($ticket->id) ? $ticket->id : null;
@endphp
<!-- Task manager table -->
<!-- Main charts -->
<div class="row">


    <div class="card col-md-12 pb-3">

        <div class="card-header header-elements-inline">
            <legend>
                {{-- <h5 class="card-title text-left">پروفایل {{ $user->nick_name . ' ' . $age }} ساله</h5> --}}
            </legend>

            <legend>
                {{-- <h5 class="card-title text-right">آخری }}</h5> --}}
            </legend>
        </div>

        <div class="card-body">
            <ul class="media-list media-chat media-chat-scrollable mb-3">


                @isset($ticket)
                <li class="media">
                    <div class="mr-3">
                        <a href="../../../../global_assets/images/placeholders/placeholder.jpg" dideo-checked="true">
                            <img src="../../../../global_assets/images/placeholders/placeholder.jpg"
                                class="rounded-circle" alt="" width="40" height="40">
                        </a>
                    </div>

                    <div class="media-body">
                        <div class="media-chat-item">{{ $ticket->content }}</div>
                        <div class="font-size-sm text-muted mt-2">{{ Verta($ticket->created_at)->format('Y/m/d H:i') }}
                            <a href="#" dideo-checked="true"><i class="icon-pin-alt ml-2 text-muted"></i></a></div>
                    </div>
                </li>
                @endisset

                @isset($tickets)

                @foreach ($tickets as $ticket)
                @if ($ticket->status == \App\Model\Ticket::STATUS_USER_RESPONSE)
                <li class="media">
                    <div class="mr-3">
                        <a href="../../../../global_assets/images/placeholders/placeholder.jpg" dideo-checked="true">
                            <img src="../../../../global_assets/images/placeholders/placeholder.jpg"
                                class="rounded-circle" alt="" width="40" height="40">
                        </a>
                    </div>

                    <div class="media-body">
                        <div class="media-chat-item">{{ $ticket->content }}</div>
                        <div class="font-size-sm text-muted mt-2">{{ Verta($ticket->created_at)->format('Y/m/d H:i') }}
                            <a href="#" dideo-checked="true"><i class="icon-pin-alt ml-2 text-muted"></i></a></div>
                    </div>
                </li>
                @elseif ($ticket->status == \App\Model\Ticket::STATUS_ADMIN_RESPONSE)

                <li class="media media-chat-item-reverse">
                    <div class="media-body">
                        <div class="media-chat-item">{{ $ticket->content }}</div>
                        <div class="font-size-sm text-muted mt-2">{{ Verta($ticket->created_at)->format('Y/m/d H:i') }}
                            <a href="#" dideo-checked="true"><i class="icon-pin-alt ml-2 text-muted"></i></a></div>
                    </div>

                    <div class="ml-3">
                        <a href="../../../../global_assets/images/placeholders/placeholder.jpg" dideo-checked="true">
                            <img src="../../../../global_assets/images/placeholders/placeholder.jpg"
                                class="rounded-circle" alt="" width="40" height="40">
                        </a>
                    </div>
                </li>
                @endif
                @endforeach

                @endisset
            </ul>

            <form action="{{ route('user.ticket.send.data') }}" method="POST">
                @csrf
                <input type="hidden" name="ticket" value="{{ $id }}">
                <textarea name="message" class="form-control mb-3" rows="3" cols="1"
                    placeholder="پیام خود را وارد کنید ..."></textarea>

                <div class="d-flex align-items-center">
                    <button type="submit" class="btn bg-teal-400 btn-labeled btn-labeled-right ml-auto"><b><i
                                class="icon-paperplane"></i></b> ارسال</button>
                </div>
            </form>

        </div>
    </div>
</div>
<!-- /task manager table -->

@endsection