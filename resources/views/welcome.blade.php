<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>وب سایت محضر</title>
    <link rel="apple-touch-icon" sizes="180x180" href="/images/all/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/all/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/all/favicons/favicon-16x16.png">
    <link rel="manifest" href="/images/all/favicons/site.webmanifest">

    <!-- plugin scripts -->

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,500,600,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="/css/all/gilroy-extrabold.css">
    <link rel="stylesheet" href="/css/all/gilroy-light.css">
    <link rel="stylesheet" href="/css/all/gilroy-semibold.css">
    <link rel="stylesheet" href="/css/all/gilroy-bold.css">

    <link rel="stylesheet" href="/css/all/animate.min.css">
    <link rel="stylesheet" href="/css/all/bootstrap.min.css">
    <link rel="stylesheet" href="/css/all/owl.carousel.min.css">
    <link rel="stylesheet" href="/css/all/owl.theme.default.min.css">
    <link rel="stylesheet" href="/css/all/magnific-popup.css">
    <link rel="stylesheet" href="/css/all/fontawesome-all.min.css">
    <link rel="stylesheet" href="/css/all/bootstrap-select.min.css">
    <link rel="stylesheet" href="/css/all/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="/css/all/bootstrap-datepicker.min.css">



    <!-- template styles -->
    <link rel="stylesheet" href="/css/all/style.css">
    <link rel="stylesheet" href="/css/all/responsive.css">


    <link rel="stylesheet" href="/css/all/color-4.css">

</head>

<body>
    <div class="preloader">
        <img src="/images/all/loader.png" class="preloader__image" alt="">
    </div><!-- /.preloader -->
    <div class="page-wrapper">

        <header class="site-header-two site-header-two__home-four">
            <nav class="main-nav__two stricky">
                <div class="container-fluid">
                    <div class="main-nav__logo-box">
                        <a href="/">
                            <img src="/images/all/logo-full-light.png" alt="">
                        </a>
                    </div><!-- /.main-nav__logo-box -->
                    <div class="main-nav__main-navigation">
                        <ul class=" main-nav__navigation-box">

                            <li>

                            </li>
                            <li>
                                <a href="/">صفحه اصلی</a>
                            </li>
                            <li>
                                <a href="contact.html">نحوه کارکرد سایت</a>
                            </li>
                            <li>
                                <a href="contact.html">تعرفه ها</a>
                            </li>

                            <li>
                                <a href="contact.html">ثبت نام دفاتر ازدواج</a>
                            </li>
                            <li>
                                <a href="contact.html">تماس با ما</a>
                            </li>
                        </ul>
                    </div><!-- /.main-nav__main-navigation -->
                    <div class="main-nav__right">
                        @if (auth('user')->check())
                        <a href="{{ route('user.index') }}" class="btn bg-transparent">پنل کاربری</a>
                        @else
                        <a href="#signup" class="btn bg-transparent">ثبت نام</a>
                        <a href="{{ route('user.login.form') }}" class="btn bg-">ورود به سایت</a>
                        @endif

                        <a href="#" class="side-menu__toggler"><span></span></a>
                    </div><!-- /.main-nav__right -->
                </div><!-- /.container-fluid -->
            </nav><!-- /.main-nav__one -->

        </header><!-- /.site-header-one -->

        <section class="banner-four">
            <img src="/images/all/rel/01.png" class="banner-four__moc" alt="">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="banner-four__content">
                            <h3>ازدواج موقت <br>
                                <span>آسان تر و مطمئن تر از قبل</span></h3>
                            <p>به سادگی با حفظ امنیت ثبت نام کنید</p>
                            <p>سپس جستجو کنید و در محل رسمی همدیگر را ملاقات کنید</p>
                            <a href="#" class="thm-btn banner-four__btn">کلیک کنید و بیشتر بدانید</a>
                            <!-- /.thm-btn banner-four__btn -->
                        </div><!-- /.banner-four__content -->
                    </div><!-- /.col-lg-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.banner-four -->





        <section class="service-five">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="service-five__single">
                            <div class="service-five__single-inner">
                                <div class="service-five__icon">
                                    <i class="far fa-chart-line"></i>
                                </div><!-- /.service-five__icon -->
                                <h3><a href="#">خصوصیات یک</a></h3>
                                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان
                                    گرافیک است چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای
                                    شرایط فعلی تکنولوژی مورد نیاز </p>
                            </div><!-- /.service-five__single-inner -->
                        </div><!-- /.service-five__single -->
                    </div><!-- /.col-lg-4 -->
                    <div class="col-lg-4">
                        <div class="service-five__single">
                            <div class="service-five__single-inner">
                                <div class="service-five__icon">
                                    <i class="far fa-rss"></i>
                                </div><!-- /.service-five__icon -->
                                <h3><a href="#">خصوصیات دو</a></h3>
                                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان
                                    گرافیک است چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای
                                    شرایط فعلی تکنولوژی مورد نیاز </p>
                            </div><!-- /.service-five__single-inner -->
                        </div><!-- /.service-five__single -->
                    </div><!-- /.col-lg-4 -->
                    <div class="col-lg-4">
                        <div class="service-five__single">
                            <div class="service-five__single-inner">
                                <div class="service-five__icon">
                                    <i class="far fa-unlock-alt"></i>
                                </div><!-- /.service-five__icon -->
                                <h3><a href="#">خصوصیات 3</a></h3>
                                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان
                                    گرافیک است چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای
                                    شرایط فعلی تکنولوژی مورد نیاز </p>
                            </div><!-- /.service-five__single-inner -->
                        </div><!-- /.service-five__single -->
                    </div><!-- /.col-lg-4 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.service-five -->



        <section class="cta-nine">
            <div class="container-fluid">
                <div class="cta-nine__block">
                    <h3>انجام کلیه امور در 4 قدم ساده </h3>
                    <p>با 4 قدم ساده عضویت خود را در سایت تکمیل کرده و کیس مورد نظر را انتخاب و قرار رسمی بگذارید</p>
                </div><!-- /.cta-nine__block -->

                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="cta-nine__single">
                            <div class="cta-nine__icon">
                                <i class="fal fa-user-alt"></i>
                                <h3>ثبت نام</h3>
                                <p> ثبت نام در سایت و تکمیل پروفایل و تائید آن از طرف مدیریت سامانه</p>
                            </div><!-- /.cta-nine__icon -->
                        </div><!-- /.cta-nine__single -->
                    </div><!-- /.col-lg-3 col-md-6 -->
                    <div class="col-lg-3 col-md-6">
                        <div class="cta-nine__single">
                            <div class="cta-nine__icon">
                                <i class="fal fa-cloud-download"></i>
                                <h3> جستجو و انتخاب </h3>
                                <p>جستجوی در سامانه و انتخاب کیس مناسب </p>
                            </div><!-- /.cta-nine__icon -->
                        </div><!-- /.cta-nine__single -->
                    </div><!-- /.col-lg-3 col-md-6 -->
                    <div class="col-lg-3 col-md-6">
                        <div class="cta-nine__single">
                            <div class="cta-nine__icon">
                                <i class="fal fa-chart-line"></i>
                                <h3> پذیرش </h3>
                                <p>ارسال درخواست به طرف مقابل و پس از پذیرش طرف مقابل</p>
                            </div><!-- /.cta-nine__icon -->
                        </div><!-- /.cta-nine__single -->
                    </div><!-- /.col-lg-3 col-md-6 -->
                    <div class="col-lg-3 col-md-6">
                        <div class="cta-nine__single">
                            <div class="cta-nine__icon">
                                <i class="fal fa-gift-card"></i>
                                <h3>قرار رسمی</h3>
                                <p>تائید از طرف مدیریت سامانه و قرار رسمی </p>
                            </div><!-- /.cta-nine__icon -->
                        </div><!-- /.cta-nine__single -->
                    </div><!-- /.col-lg-3 col-md-6 -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->

        </section><!-- /.cta-nine -->

        <section class="cta-six" style="background-image: url(/images/all/backgrounds/home-4-bubbled-bg.png);">
            <img src="/images/all/rel/02.png" class="cta-six__moc" alt="">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7">

                    </div><!-- /.col-lg-5 -->
                    <div class="col-lg-5">
                        <form method="POST" action="{{ route('user.signup') }}" id="signup">
                            @csrf

                            <div class="form-group row">
                                <label for="nick_name"
                                    class="col-md-4 col-form-label text-md-right">{{ __('auth.nick_name') }}</label>

                                <div class="col-md-6">
                                    <input id="nick_name" type="text"
                                        class="form-control @error('nick_name') is-invalid @enderror" name="nick_name"
                                        value="{{ old('nick_name') }}" required autocomplete="nick_name"
                                        {{ ($errors->any() ? 'autofocus' : '') }}>

                                    @error('nick_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="mobile"
                                    class="col-md-4 col-form-label text-md-right">{{ __('auth.gender') }}</label>

                                <div class="col-md-6">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="gender" id="female" value="0"
                                            checked>
                                        <label class="form-check-label pr-4" for="female">
                                            خانم
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="gender" id="man" value="1">
                                        <label class="form-check-label pr-4" for="man">
                                            آقا
                                        </label>
                                    </div>
                                    @error('gender')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror

                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="mobile"
                                    class="col-md-4 col-form-label text-md-right">{{ __('auth.mobile') }}</label>

                                <div class="col-md-6">
                                    <input id="mobile" type="text"
                                        class="form-control @error('mobile') is-invalid @enderror" name="mobile"
                                        value="{{ old('mobile') }}" required autocomplete="mobile">

                                    @error('mobile')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password"
                                    class="col-md-4 col-form-label text-md-right">{{ __('auth.password') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password"
                                        class="form-control @error('password') is-invalid @enderror" name="password"
                                        required autocomplete="new-password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password-confirm"
                                    class="col-md-4 col-form-label text-md-right">{{ __('auth.repeat_password') }}</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control"
                                        name="password_confirmation" required autocomplete="new-password">
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('auth.register') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div><!-- /.col-lg-5 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.cta-six -->

        <footer class="site-footer-four">
            <div class="site-footer-four__bubble-1"></div><!-- /.site-footer-four__bubble-1 -->
            <div class="site-footer-four__bubble-2"></div><!-- /.site-footer-four__bubble-2 -->
            <div class="site-footer-four__bubble-3"></div><!-- /.site-footer-four__bubble-3 -->
            <div class="site-footer-four__bubble-4"></div><!-- /.site-footer-four__bubble-4 -->
            <div class="site-footer-four__upper">
                <div class="container">
                    <div class="site-footer-four__upper-sep"></div><!-- /.site-footer-four__upper-sep -->
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="footer-widget footer-widget__about">
                                <a href="index.html">
                                    <img src="/images/all/logo-full-light.png" alt="">
                                </a>
                                <p>شعار سایت در این قسمت قرار می گیرد</p>
                            </div><!-- /.footer-widget footer-widget__about -->
                        </div><!-- /.col-lg-4 -->
                        <div class="col-lg-4">
                            <div class="footer-widget footer-widget__links">
                                <h3 class="footer-widget__title">لینک های مفید</h3><!-- /.footer-widget__title -->
                                <ul class="list-unstyled">
                                    <li><a href="#">لینک</a></li>
                                    <li><a href="#">لینک</a></li>
                                    <li><a href="#">لینک</a></li>
                                    <li><a href="#">لینک</a></li>
                                    <li><a href="#">لینک</a></li>
                                    <li><a href="#">لینک</a></li>
                                </ul><!-- /.list-unstyled -->
                            </div><!-- /.footer-widget footer-widget__links -->
                        </div><!-- /.col-lg-4 -->
                        <div class="col-lg-4">
                            <h3 class="footer-widget__title">آخرین اخبار سایت</h3><!-- /.footer-widget__title -->
                            <div class="sidebar__post-single">
                                <div class="sidebar__post-image">
                                    <img src="/images/all/blog/blog-lp-1-1.jpg" alt="">
                                </div><!-- /.sidebar__post-image -->
                                <div class="sidebar__post-content">
                                    <h3 style="color: white"><a href="#">اولین خبر تست این سایت می باشد</a></h3>
                                    <span style="color: white">1399/01/25</span>
                                </div><!-- /.sidebar__post-content -->
                            </div><!-- /.sidebar__post-single -->
                            <div class="sidebar__post-single">
                                <div class="sidebar__post-image">
                                    <img src="/images/all/blog/blog-lp-1-1.jpg" alt="">
                                </div><!-- /.sidebar__post-image -->
                                <div class="sidebar__post-content">
                                    <h3 style="color: white"><a href="#">دومین خبر تست این سایت می باشد</a></h3>
                                    <span style="color: white">1399/01/25</span>
                                </div><!-- /.sidebar__post-content -->
                            </div><!-- /.sidebar__post-single -->
                        </div>
                    </div><!-- /.col-lg-4 -->
                </div><!-- /.row -->
            </div><!-- /.container -->


            <div class="site-footer-four__bottom">
                <div class="container text-center">
                    <div class="site-footer-four__bottom-sep"></div><!-- /.site-footer-four__upper-sep -->
                    <p>Copy@2020 <a href="#">rubik</a>. All Right Reserved.</p>
                </div><!-- /.container -->
            </div><!-- /.site-footer-four__bottom -->
        </footer><!-- /.site-footer-four -->

    </div><!-- /.page-wrapper -->


    <a href="#" data-target="html" class="scroll-to-target scroll-to-top"><i class="fa fa-angle-up"></i></a>


    <div class="side-menu__block">


        <div class="side-menu__block-overlay custom-cursor__overlay">
            <div class="cursor"></div>
            <div class="cursor-follower"></div>
        </div><!-- /.side-menu__block-overlay -->
        <div class="side-menu__block-inner ">
            <div class="side-menu__top justify-content-end">

                <a href="#" class="side-menu__toggler side-menu__close-btn"><img src="/images/all/shapes/close-1-1.png"
                        alt=""></a>
            </div><!-- /.side-menu__top -->


            <nav class="mobile-nav__container">
                <!-- content is loading via js -->
            </nav>
            <div class="side-menu__sep"></div><!-- /.side-menu__sep -->
            <div class="side-menu__content">
                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است چاپگرها و
                    متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز</p>
                <p><a href="mailto:needhelp@bizkar.com">needhelp@mebadi.ir</a> <br> <a href="tel:888-999-0000">888 999
                        0000</a></p>
                <div class="side-menu__social">
                    <a class="fab fa-facebook-f" href="#"></a>
                    <a class="fab fa-twitter" href="#"></a>
                    <a class="fab fa-instagram" href="#"></a>
                    <a class="fab fa-pinterest-p" href="#"></a>
                </div>
            </div><!-- /.side-menu__content -->
        </div><!-- /.side-menu__block-inner -->
    </div><!-- /.side-menu__block -->



    <div class="search-popup" style="display: none;">
        <div class="search-popup__overlay custom-cursor__overlay">
            <div class="cursor"></div>
            <div class="cursor-follower"></div>
        </div><!-- /.search-popup__overlay -->
        <div class="search-popup__inner">
            <form action="#" class="search-popup__form">
                <input type="text" name="search" placeholder="Type here to Search....">
                <button type="submit"><i class="fa fa-search"></i></button>
            </form>
        </div><!-- /.search-popup__inner -->
    </div><!-- /.search-popup -->


    <script src="/js/all/jquery.min.js"></script>
    <script src="/js/all/bootstrap.bundle.min.js"></script>
    <script src="/js/all/owl.carousel.min.js"></script>
    <script src="/js/all/waypoints.min.js"></script>
    <script src="/js/all/jquery.counterup.min.js"></script>
    <script src="/js/all/TweenMax.min.js"></script>
    <script src="/js/all/wow.js"></script>
    <script src="/js/all/jquery.magnific-popup.min.js"></script>
    <script src="/js/all/jquery.ajaxchimp.min.js"></script>
    <script src="/js/all/jquery.validate.min.js"></script>
    <script src="/js/all/bootstrap-select.min.js"></script>
    <script src="/js/all/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="/js/all/bootstrap-datepicker.min.js"></script>
    <script src="/js/all/isotope.js"></script>

    <!-- template scripts -->
    <script src="/js/all/theme.js"></script>
</body>

</html>