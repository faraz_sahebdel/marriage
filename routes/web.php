<?php

use App\Model\Admin;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Spatie\Activitylog\Models\Activity;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// dd(Auth::guard('admin')->check());
// 




Route::get('/', function () {

    return Activity::all()->last();


    // Admin::create([
    //     'name' => 'faraz',
    //     'email' => 'admin@gmail.com',
    //     'password' => Hash::make(123456789),
    // ]);

    // User::create([
    //     'nick_name' => 'ali reza',
    //     'mobile' => '09357589031',
    //     'gender' => 1,
    //     'password' =>Hash::make(123456)
    // ]);
    return view('welcome');
});


// Auth::routes();

Route::namespace('Auth')->group(function () {

    // activity()->log('Look mum, I logged something')->su;

    Route::get('/admin/login', 'LoginController@showAdminLoginForm')->name('admin.login.form');
    Route::get('/user/login', 'LoginController@showUserLoginForm')->name('user.login.form');
    Route::get('/office/login', 'LoginController@showOfficeLoginForm')->name('office.login.form');

    // Route::get('/user/signup', 'RegisterController@showRegistrationForm')->name('user.signup.form');

    Route::post('/admin/login', 'LoginController@adminLogin')->name('admin.login');
    Route::post('/user/login', 'LoginController@userLogin')->name('user.login');
    Route::post('/office/login', 'LoginController@officeLogin')->name('office.login');

    Route::get('/admin/logout', 'LoginController@adminLogout')->name('admin.logout');
    Route::get('/user/logout', 'LoginController@userLogout')->name('user.logout');
    Route::get('/office/logout', 'LoginController@officeLogout')->name('office.logout');

    Route::get('/office/signup', 'RegisterController@showOfficeRegistrationForm')->name('office.signup');

    // Route::post('/register/admin', 'RegisterController@createAdmin');
    Route::post('/signup', 'RegisterController@register')->name('user.signup');
    Route::post('/office/signup', 'RegisterController@officeRegister')->name('office.signup');


    Route::get('/password/reset', 'ResetPasswordController@showResetForm')->name('password.request');
    Route::post('/password/update', 'ResetPasswordController@reset')->name('password.update');

    Route::get('/token/reset/{mobile}', 'ResetPasswordController@showTokenForm')->name('password.reset.token');
    Route::post('/token/update', 'ResetPasswordController@tokenConfirmed')->name('password.update.token')->middleware('throttle:5|1');



    // Route::view('/home', 'home')->middleware('auth');

    // Route::view('/admin/dashboard', 'admin')->name('admin.');
    // Route::view('/writer', 'writer');


});


Route::namespace('Admin')->prefix('admin')->name('admin.')->middleware('authDefaultDriver')->group(function () {

    Route::get('/', 'AdminController@index')->name('index');

    Route::get('/setting', 'AdminController@setting')->name('setting');
    Route::post('/reset', 'AdminController@resetPassword')->name('password.reset');

    Route::resource('user', 'UserController');
    Route::get('users', 'UserController@showDataTable')->name('user.list');

    Route::get('financial/all', 'AdminController@financialAll')->name('financial.all');

    Route::get('financial/success', 'AdminController@financialSuccess')->name('financial.success');
    Route::get('financial/success/list', 'AdminController@showDataTableFinancialSuccess')->name('financial.success.list');

    Route::get('financial/fail', 'AdminController@financialFail')->name('financial.fail');
    Route::get('financial/fail/list', 'AdminController@showDataTableFinancialFail')->name('financial.fail.list');

    Route::get('alert/index', 'AlertController@index')->name('alert.index');
    Route::get('alert/list', 'AlertController@showDataTableAlert')->name('alert.list');
    Route::get('alert/create', 'AlertController@create')->name('alert.create');
    Route::post('alert/store', 'AlertController@store')->name('alert.store');
    Route::get('alert/destroy/{alert}', 'AlertController@destroy')->name('alert.destroy');

    Route::resource('admins', 'AdminManagerController');
    Route::get('admins-list', 'AdminManagerController@showDataTable')->name('admins.list');

    Route::get('admins/permission/{admin}', 'AdminManagerController@permission')->name('admins.permission');
    Route::post('admins/permission/{admin}', 'AdminManagerController@permissionUpdate')->name('admins.permission');

    Route::get('reports', 'ReportController@index')->name('report.index');
    Route::get('report-list', 'ReportController@showDataTable')->name('report.list');

    Route::get('blocks', 'BlockController@index')->name('block.index');
    Route::get('block-list', 'BlockController@showDataTable')->name('block.list');

    Route::get('admin/blocks', 'BlockController@adminBlockIndex')->name('admin.block');
    Route::get('admin/block-list', 'BlockController@adminBlockShowDataTable')->name('admin.block.list');
    Route::get('admin/block/delete/{user}', 'BlockController@adminBlockDelete')->name('admin.block.delete');

    Route::post('admin/blocks/add/{user}', 'BlockController@adminBlockAdd')->name('admin.block.add');

    Route::resource('package', 'PackageController');


    Route::get('config', 'ConfigController@index')->name('config');
    Route::post('config', 'ConfigController@update')->name('config.update');


    Route::get('ticket/index', 'TicketController@index')->name('ticket.index.list');
    Route::get('ticket/list', 'TicketController@showDataTableTicket')->name('ticket.index.lists');

    Route::get('ticket/send/{ticket}', 'TicketController@send')->name('ticket.send');
    Route::post('ticket/send/data', 'TicketController@sendData')->name('ticket.send.data');

    Route::get('request', 'RequestController@index')->name('request.index');
    Route::get('request/list', 'RequestController@showDataTableRequest')->name('request.list');

    Route::get('request/{request}', 'RequestController@officeSend')->name('request.offuce.send');
    Route::post('request/{model_request}', 'RequestController@officeSendSave')->name('request.offuce.save');

    Route::resource('office', 'OfficeController');
    Route::get('office-list', 'OfficeController@showDataTable')->name('office.list');

    Route::get('log', 'LogController@index')->name('log.index');
    Route::get('log-list', 'LogController@showDataTable')->name('log.list');

    Route::get('export', 'LogController@export')->name('log.export');

});

Route::namespace('User')->prefix('user')->name('user.')->middleware('authDefaultDriver')->group(function () {

    Route::get('order/callback/{id}', 'PackageController@callback')->name('order.callback');

    Route::get('order/index', 'OrderController@index')->name('order.index');
    Route::get('order/list', 'OrderController@showDataTableOrder')->name('order.list');

    Route::get('package', 'PackageController@index')->name('package');
    Route::post('package/buy/{package}', 'PackageController@packageBuy')->name('package.buy');

    Route::get('favorite', 'RequestController@favorite')->name('favorite.index');
    Route::get('favorite/list', 'RequestController@showDataTableFavorite')->name('favorite.list');

    Route::get('block', 'RequestController@block')->name('block.index');
    Route::get('block/list', 'RequestController@showDataTableBlock')->name('block.list');
    Route::get('block/delete/{block}', 'RequestController@blockDelete')->name('block.delete');

    Route::get('/', 'UserController@index')->name('index');
    Route::post('/enable/code', 'UserController@enableCode')->name('enable.code');

    Route::get('/info', 'UserController@infoUpdateForm')->name('info.update');
    Route::post('/info', 'UserController@infoSave')->name('info.save');

    Route::get('/setting', 'UserController@setting')->name('setting');
    Route::post('/reset', 'UserController@resetPassword')->name('password.reset');

    Route::get('spouse', 'SpouseController@index')->name('spouse');

    Route::get('spouse/list', 'SpouseController@showDataTable')->name('spouse.list');

    Route::get('{user}', 'SpouseController@userShow')->name('show');

    Route::post('request/{user}', 'SpouseController@userRequest')->name('user.request');

    Route::post('user/request/like', 'SpouseController@userRequestLike')->name('request.like');

    Route::post('user/block/{user}', 'SpouseController@userBlock')->name('block');

    Route::post('user/report', 'SpouseController@userReport')->name('report');

    Route::get('request/get', 'RequestController@requestGet')->name('request.get');
    Route::get('request/get/list', 'RequestController@showDataTableRequestGet')->name('request.get.list');

    Route::get('request/send', 'RequestController@requestSend')->name('request.send');


    Route::get('request/send', 'RequestController@requestSend')->name('request.send');
    Route::get('request/send/list', 'RequestController@showDataTableRequestSend')->name('request.send.list');

    Route::get('ticket/index', 'TicketController@index')->name('ticket.index.list');
    Route::get('ticket/list', 'TicketController@showDataTableTicket')->name('ticket.index.lists');

    Route::get('ticket/send/{ticket?}', 'TicketController@send')->name('ticket.send');
    Route::post('ticket/send/data', 'TicketController@sendData')->name('ticket.send.data');
});


Route::namespace('Office')->prefix('office')->name('office.')->middleware('authDefaultDriver')->group(function () {

    Route::get('/', 'OfficeController@index')->name('index');

    Route::get('info', 'OfficeController@info')->name('info');
    Route::post('info', 'OfficeController@infoUpdate')->name('info.update');

    Route::get('request', 'RequestController@index')->name('request.index');
    Route::get('request/list', 'RequestController@showDataTableRequest')->name('request.list');
});
